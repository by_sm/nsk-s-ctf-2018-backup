<?=tpl_load('print-gemeister.php', array(
	'messages' => $messages
))?>
<?
/*
<? if($_GET['showhidden']){?>
<!--<a href="?<?=rtl_get_merge_parameters([], ['showhidden'=>0], TRUE)?>">Скрыть удалённые</a>
<? }else{?>
<a href="?<?=rtl_get_merge_parameters([], ['showhidden'=>1], TRUE)?>">Показать удалённые</a>
<?}?> |

*/?>
<a href="/hauptmeister/haupteditteam">Новая команда</a> |
<a onclick="return confirm('Вы точно хотите БЕЗВОЗВРАТНО удалить все помеченные удаленными команды, имеющие последние изменения старше 7 дней?');" href="?<?=rtl_get_merge_parameters(['showhidden'], ['garbagecollect'=>1], TRUE)?>">GarbageCollect</a>
<div>Новые &mdash; вверху</div>

<h3>Команды NSK SCTF 2017 (<?=is_array($teams_school)?count($teams_school):0?>)</h3>
<div>
	<? if($teams_school){?>
		<table class="h-getable">
			<tr>
				<th>Logo</th>
				<th>Teamname</th>
				<th>Leader</th>
				<th>Members</th>
				<th>School</th>
				<th>Class</th>
				<th>Contacts</th>
				<th>Actionz</th>
				<th>Date reg</th>
				<th>Contest User</th>
			</tr>
		<? foreach($teams_school as $zeile){ ?>
			<tr class="<?=$zeile->is_removed ? 'tr-wiped' : ''?>">
				<td>
					<? if($zeile->logo_path){?>
					<a href="<?=$zeile->logo_path?>" target="_blank"><img class="h-team-logo" src="<?=$zeile->logo_path?>" /></a>
					<? }else{ ?>
					<i>Нема</i>
					<? }?>
				</td>
				<td><?=$zeile->name_team?></td>
				<td><?=$zeile->fio_captain?></td>
				<td><?=nl2br($zeile->fio_alles)?></td>
				<td><?=$zeile->school?></td>
				<td><?=$zeile->klasse?></td>
				<td><?=$zeile->contacts?></td>
				<td>
					<a href="/hauptmeister/haupteditteam/<?=$zeile->id?>">EDIT</a>
					<a href="?<?=rtl_get_merge_parameters(['showhidden'], [($zeile->is_removed?'rsteam':'rmteam')=>$zeile->id], TRUE)?>" onclick="return confirm('А вы хорошо подумали?');"><?=$zeile->is_removed?'rs':'rm'?></a>
				</td>
				<td><?=$zeile->date_reg?></td>
				<td>
					<? if($zeile->lg){?>
					<a class="<?=$zeile->is_cuser_removed ? 'h-a-wiped' : ''?>" href="/hauptmeister/contest/useredit/vorzeigen/<?=$zeile->id_cuser?>"><?=$zeile->lg?></a>
					<?}else{?>
					<a href="/hauptmeister/contest/useredit/erstellen/<?=$zeile->id?>">[запилить]</a>
					<?}?>
				</td>
			</tr>
		<? }?>
		</table>
	<? }else{?>
		Ничего тут нет
	<? }?>
</div>

<h3>Команды NSK CTF 2017 (<?=is_array($teams_sibsut)?count($teams_sibsut):0?>)</h3>
<div>
	<? if($teams_sibsut){?>
		<table class="h-getable">
			<tr>
				<th>Logo</th>
				<th>Teamname</th>
				<th>Leader</th>
				<th>Members</th>
				<th>Univer</th>
				<th>Course</th>
				<th>Contacts</th>
				<th>Actionz</th>
				<th>Date reg</th>
				<th>Contest User</th>
			</tr>
		<? foreach($teams_sibsut as $zeile){ ?>
			<tr class="<?=$zeile->is_removed ? 'tr-wiped' : ''?>">
				<td>
					<? if($zeile->logo_path){?>
					<a href="<?=$zeile->logo_path?>" target="_blank"><img class="h-team-logo" src="<?=$zeile->logo_path?>" /></a>
					<? }else{ ?>
					<i>Нема</i>
					<? }?>
				</td>
				<td><?=$zeile->name_team?></td>
				<td><?=$zeile->fio_captain?></td>
				<td><?=nl2br($zeile->fio_alles)?></td>
				<td><?=$zeile->school?></td>
				<td><?=$zeile->klasse?></td>
				<td><?=$zeile->contacts?></td>
				<td>
					<a href="/hauptmeister/haupteditteam/<?=$zeile->id?>">EDIT</a>
					<a href="?<?=rtl_get_merge_parameters(['showhidden'], [($zeile->is_removed?'rsteam':'rmteam')=>$zeile->id], TRUE)?>" onclick="return confirm('А вы хорошо подумали?');"><?=$zeile->is_removed?'rs':'rm'?></a>
				</td>
				<td><?=$zeile->date_reg?></td>
				<td>
					<? if($zeile->lg){?>
					<a class="<?=$zeile->is_cuser_removed ? 'h-a-wiped' : ''?>" href="/hauptmeister/contest/useredit/vorzeigen/<?=$zeile->id_cuser?>"><?=$zeile->lg?></a>
					<?}else{?>
					<a href="/hauptmeister/contest/useredit/erstellen/<?=$zeile->id?>">[запилить]</a>
					<?}?>
				</td>
			</tr>
		<? }?>
		</table>
	<? }else{ ?>
		Ничего тут нет
	<? }?>
</div>
<?=tpl_load('print-gemeister.php', array(
	'messages' => $messages
))?>
