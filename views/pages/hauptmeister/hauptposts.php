<?=tpl_load('print-gemeister.php', array(
	'messages' => $messages
))?>

<div>
<? if($_GET['showhidden']){?>
<a href="?<?=rtl_get_merge_parameters([], ['showhidden'=>0], TRUE)?>">Скрыть удалённые</a>
<? }else{?>
<a href="?<?=rtl_get_merge_parameters([], ['showhidden'=>1], TRUE)?>">Показать удалённые</a>
<?}?> |
<a onclick="return confirm('Вы точно хотите БЕЗВОЗВРАТНО удалить все помеченные удаленными посты, имеющие последние изменения старше 7 дней?');" href="?<?=rtl_get_merge_parameters(['showhidden'], ['garbagecollect'=>1], TRUE)?>">GarbageCollect</a>
</div>
<div>Новые посты &mdash; вверху</div>

<? foreach($epota as $reihe){ ?>
<div class="h-epage <?=$reihe->is_disabled ? 'hc-inactive' : ''?>">
	<h2><?=$reihe->alias?> &Rarr; <?=$reihe->title?> &Rarr; <?=$reihe->h1ead?></h2>
	<div>
		<a href="/hauptmeister/haupteditpost/new/<?=$reihe->id ?>">Новый пост</a> |
		<a href="/hauptmeister/haupteditpage/<?=$reihe->id ?>">Редактировать</a> |
		<?=$reihe->is_disabled ? 'Неактивна' : 'Активна' ?>
	</div>
	<? if($reihe->eposts){ ?>
		<table class="h-getable">
			<tr>
				<th>ID</th>
				<th>Title</th>
				<th>Date</th>
				<th>Actionz</th>
			</tr>
			<? foreach($reihe->eposts as $zeile){ ?>
			<tr class="<?=$zeile->is_removed ? 'tr-wiped' : ''?>">
				<td><?=$zeile->id ?></td>
				<td><?=$zeile->heading ?></td>
				<td><?=$zeile->date_post ?></td>
				<td>
					<a href="/hauptmeister/haupteditpost/<?=$zeile->id ?>">EDIT</a>
					<a href="?<?=rtl_get_merge_parameters(['showhidden'], [($zeile->is_removed?'rspost':'rmpost')=>$zeile->id], TRUE)?>" onclick="return confirm('А вы хорошо подумали?');"><?=$zeile->is_removed?'rs':'rm'?></a>
				</td>
			</tr>
			<? } ?>
		</table>
	<? }else{ ?>
	<div>Постов нет</div>
	<? } ?>
</div>
<? } ?>
<?=tpl_load('print-gemeister.php', array(
	'messages' => $messages
))?>