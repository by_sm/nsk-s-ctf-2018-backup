<?

define('RTL_REGEXP_EMAIL', '/^[\w\.\-]+\@[\w\.\-]{1,64}\.\w{2,6}$/'); //регулярка проверяющая на валдидность емейл. хитрая регцулаяка
define('RTL_REGEXP_PASSWORD', '/^.{6,255}$/');
define('RTL_REGEXP_DATETIME', '/^\d{4}\-\d{2}\-\d{2} \d{2}\:\d{2}\:\d{2}$/'); //валидная датавремя
define('RTL_REGEXP_DATE', '/^\d{4}\-\d{2}\-\d{2}$/'); //валидная дата, только дата
define('RTL_REGEXP_TIME', '/^\d{2}\:\d{2}\:\d{2}$/'); //валидная итолько время
define('RTL_REGEXP_VARNAME', '/^[\w\.\-]{1,255}$/'); //для очищения имен переменных от всякой хуеты
define('RTL_REGEXP_MD5', '/^[a-f0-9]{32}$/i'); //валидный md5 хещь (без учета регистра)
define('RTL_REGEXP_SHA1', '/^[a-f0-9]{40}$/i'); //как sha1 так и ripemd160, например
define('RTL_REGEXP_SHA256', '/^[a-f0-9]{64}$/i'); //валидный sha-256 хещь (без учета регистра)
define('RTL_REGEXP_NICK', '/^.{1,64}$/iu'); //валидный ник юзера, и вообще /*(юникодный модификатор жаваскрипт не переваривает. Fail)*/ Это ты сплошеой фейл, дружок
define('RTL_REGEXP_LINK', '/^https?\:\/\/[\w\.\-\:]*\@?[\w\.\-]*/iu');
define('RTL_REGEXP_INT', '/^\d{1,18}$/');
define('RTL_REGEXP_FLOAT', '/^(\d{1,16}|\d{1,16}\.\d{1,16}|\d{1,16}\.\d{1,16}e\-?\d{1,16})$/iu');  //(?:\.\d{1,16})?
define('RTL_REGEXP_SOCKNAME', '/^(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\:(\d{1,5})$/');
define('RTL_REGEXP_IP', '/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/');
define('RTL_REGEXP_MAC', '/^(?:[0-9A-F]{2}\:){5}[0-9A-F]{2}$/');
define('RTL_REGEXP_FMT_ZIP', '/^\x50\x4b\x03\x04/');
define('RTL_REGEXP_FMT_GZIP', '/^\x1f\x8b\x08/');
define('RTL_REGEXP_FMT_PNG', '/^\x89PNG/');
define('RTL_REGEXP_2HOST', '/^[\w\-]+\.[\w\-]+$/');
define('RTL_REGEXP_XHOST', '/^[\w\-\.]+\.[\w\-]+$/');
define('RTL_REGEXP_EOLMATCH', '/\r\n|\n/s');
define('RTL_REGEXP_PHONE', '#^\+\d{10,12}$#');
define('RTL_REGEXP_KPP', '#^\d{4}[0-9A-Z]{2}\d{3}$#');
define('RTL_REGEXP_ACCOUNT', '#^\d{20}#');

define('O_DINT_SECOND',		1);
define('O_DINT_MINUTE',		2);
define('O_DINT_HOUR',		4);
define('O_DINT_DAY',		8);
define('O_DINT_YEAR',		16);

define('O_DINT_ALL',		1 | 2 | 4 | 8 | 16);


define('O_DALW_SECOND',		32);
define('O_DALW_MINUTE',		64);
define('O_DALW_HOUR',		128);
define('O_DALW_DAY',		256);
define('O_DALW_YEAR',		512);

class CKernel {

	const ERROR_LEVEL_DEBUG =	0;
	const ERROR_LEVEL_INFO =	1;
	const ERROR_LEVEL_WARNING = 2;
	const ERROR_LEVEL_FAILURE = 3;

	public static $db_link;
	public static $config;
	public static $profile;

	private static $errors = array();

	public static $request = array();
	public static $reqpath = array();

	public static function KeFailure($http_message, $http_code){
		echo $http_message;
	}

	public static function KeContinue($default = FALSE){

		$cnext = count(self::$request) > 0 ? array_shift(self::$request) : $default;

		if($cnext !== FALSE){
			$poth = $_SERVER['DOCUMENT_ROOT'] . '/core/logic/' . implode('/', self::$reqpath) . (count(self::$reqpath) > 0 ? '/' : '' ) . "$cnext.php";

			if(is_file($poth)){

				self::$reqpath[] = $cnext;
				include $poth;
			}else{
				self::KeFailure('Страница не найдена', 404);
			}

		}else{
			self::KeFailure();
		}
	}

	public static function KeGetMessages(){
		return self::$errors;
	}

	public static function KeClearMessages(){
		self::$errors = array();
	}

	public static function KeRegisterMessage($message, $level = 4){
		self::$errors[] = (object) [
			'message' => $message,
			'level' => $level
		];
	}

	public static function KeSaveMessages(){
		if(!session_id()) return FALSE;
		$_SESSION['mesaroes'] = self::$errors;
		return TRUE;
	}

	public static function KeRestoreMessages(){
		if(!session_id()) return FALSE;
		if(!is_array($_SESSION['mesaroes'])) return FALSE;

		self::$errors = array_merge(self::$errors, $_SESSION['mesaroes']);
		$_SESSION['mesaroes'] = [];
	}

	public static function KeAssert($samojebka, $message, $level = 4){
		if(!$samojebka) self::KeRegisterMessage($message, $level);
		return !$samojebka;
	}

	public static function AnalyzeRequest(){

		$argpos = strpos($_SERVER['REQUEST_URI'], '?');
		$heshpo = strpos($_SERVER['REQUEST_URI'], '#');

		if(FALSE === $argpos) $argpos = 1 << 31;
		if(FALSE === $heshpo) $heshpo = 1 << 31;

		$ohne = min($argpos, $heshpo);

		if(1 << 31 == $ohne) $ohne = FALSE;

		self::$request = array();

		$das_request = array_map('trim', explode('/', trim(FALSE !== $ohne ? substr($_SERVER['REQUEST_URI'], 0, $ohne) : $_SERVER['REQUEST_URI'], '/')));

		foreach($das_request as $teil){
			if(!preg_match('#^\.+$|^\s*$#', $teil)){
				self::$request[] = $teil;
			}
		}

		self::$reqpath = array();
	}

	public static function KeReadVariable($namen){
		return rtl_database_select('cv.daten', 'ctf_vars cv', FALSE, array(
			"namen='$namen'"
		), FALSE, FALSE, 0, 1, $found_rows, O_SELECT_ONE_VALUE);
	}

	public static function KeUpdateVariable($namen, $daten){
		rtl_database_insert('ctf_vars', array(
			'namen' => $namen,
			'daten' => $daten
		), O_INSERT_REPLACE);
	}
}

define('O_INSERT_REPLACE', 1);
define('O_INSERT_DELAYED', 2);
define('O_INSERT_IGNORE', 4);

define('O_INSERT_MULTI', 8);
define('O_INSERT_GET_RAW_SQL', 32);

define('O_SELECT_CALC_FOUND_ROWS', 1);

define('O_SELECT_FIRST_ROW', 2);
define('O_SELECT_ONE_VALUE', 4);

define('O_SELECT_FORCE_CACHE', 8);
define('O_SELECT_FORCE_NOCACHE', 16);
define('O_SELECT_GET_RAW_SQL', 32);

function lib_load($u_lib){
	return include_once "$_SERVER[DOCUMENT_ROOT]/core/lib/$u_lib";
}

function tpl_load($u_template, $vparams = NULL){
	$vparams || $vparams = array();
	ob_start();
	extract($vparams);
	unset($vparams);
	include "$_SERVER[DOCUMENT_ROOT]/views/$u_template";
	return ob_get_clean();
}

function rtl_config_parse(){
	require_once "$_SERVER[DOCUMENT_ROOT]/core/config.php";
	
	$config || $config = array();
	
	CKernel::$config = $config;
}

function rtl_database_connect(){
	
	if(!$t_database_link = mysqli_connect(CKernel::$config['mdb-host'], CKernel::$config['mdb-user'], CKernel::$config['mdb-password'])){
		// error
		return FALSE;
	}

	if(!mysqli_select_db($t_database_link, CKernel::$config['mdb-database'])){
		// error
		return FALSE;
	}

	if(!mysqli_query($t_database_link, $t_query = "SET NAMES 'utf8';")){
		// error
		return FALSE;
	}

	CKernel::$db_link = $t_database_link;

	return TRUE;
}



function rtl_sqlc_escape($w_param){
	if(is_int($w_param) || is_float($w_param)){
		return strval($w_param);
	}
	if($w_param === NULL){
		return 'NULL';
	}
	if(is_bool($w_param)){
		return strval(intval($w_param));
	}
	is_string($w_param) || $w_param = rtl_nuclear_serialize($w_param);
	return "'" .addslashes($w_param) . "'";
}


function rtl_sqlc_select_fields($u_fields_select = FALSE){
	if($u_fields_select === FALSE){
		return '';
	}

	if(is_string($u_fields_select)){
		return $u_fields_select;
	}
	if(is_array($u_fields_select)){
		foreach($u_fields_select as $k_alias => &$t_expr){
			is_string($k_alias) && $t_expr = "$t_expr AS `$k_alias`";
		}
	}
	return implode(',', $u_fields_select);
}


function rtl_sqlc_json_field($u_fields, $s_expr_key = NULL){

	$l_fields = array();

	foreach($u_fields as $k_alias => $t_expression){
		is_int($k_alias) && $k_alias = $t_expression;

		$l_typevar = 0;

		if($n_micropos = strpos($k_alias, '#')){
			$k_alias[$n_micropos + 1] == 'i' && $l_typevar = 1;
			$k_alias[$n_micropos + 1] == 'u' && $l_typevar = 2;

			$k_alias = substr($k_alias, 0, $n_micropos);
		}

		switch($l_typevar){
			case 0:
				$l_fields[] = "'\"$k_alias\":\"', IFNULL(REPLACE(REPLACE(REPLACE(REPLACE($t_expression, '\\\x5C', '\\\x5C\\\x5C'), '\"', '\\\x5C\"'), '\\n', '\\\x5Cn'), '\\r', '\\\x5Cr'), ''), '\"'";
			break;
			case 1:
				$l_fields[] = "'\"$k_alias\":', IFNULL($t_expression, 'null')";
			break;
			case 2:
				$l_fields[] = "'\"$k_alias\":', IFNULL(CAST($t_expression AS SIGNED), 0)";
			break;
		}

	}

	$l_fields = implode(", ',', ", $l_fields);
	$s_fragment_index = $s_expr_key ? "'\"', $s_expr_key, '\":', " : '';
	$s_frag_bleft = $s_expr_key ? '{' : '[';
	$s_frag_bright = $s_expr_key ? '}' : ']';

	return "CONCAT('$s_frag_bleft', GROUP_CONCAT(CONCAT($s_fragment_index'{', $l_fields, '}') SEPARATOR ','), '$s_frag_bright')";
}

function rtl_utils_number_xsplit($n_int, $wsp = '`'){
	$x = strrev(chunk_split(strrev((string) (int) $n_int), 3, strrev($wsp)));
	if(substr($x, 0, strlen($wsp)) == $wsp) $x = substr($x, strlen($wsp));
	return $x;
}

function rtl_sqlc_in($u_data, $is_convert_to_numbers = FALSE){
	count($u_data) || $u_data = array(0);
	$is_convert_to_numbers && $u_data = array_map('intval', $u_data);
	$u_data = array_map('rtl_sqlc_escape', $u_data);
	return sprintf('IN(%s)', implode(',', $u_data));
}


function rtl_sqlc_join($u_data_join = FALSE){
	if($u_data_join === FALSE){
		return '';
	}

	if(is_string($u_data_join)){
		return $u_data_join;
	}

	$r_join = '';

	if(is_array($u_data_join)){
		foreach($u_data_join as $k_fields => $t_join){
			$r_join .= " $k_fields ";
			$t_join && $r_join .= 'ON '. (is_array($t_join) ? implode(' && ', $t_join) : $t_join);
		}
	}

	return $r_join;
}


function rtl_sqlc_update($u_data){
	$l_qr = array();
	foreach($u_data as $k_name => $t_value){
		$t_names = explode('.', $k_name);
		$u_names = array();
		foreach($t_names as $t_piece){
			$t_piece != '' && $u_names[] = "`$t_piece`";
		}

		$u_key = implode('.', $u_names);
		$l_qr[] = "$u_key=".($t_names[0] == '' ? $t_value : rtl_sqlc_escape($t_value));
	}
	return implode(',', $l_qr);
}

function rtl_sqlc_insert_values($u_data){
	$l_param_names = array();
	$l_param_values = array();
	foreach($u_data as $k_name => $t_value){
		$t_names = explode('.', $k_name);
		$u_names = array();
		foreach($t_names as $t_piece){
			$t_piece != '' && $u_names[] = "`$t_piece`";
		}
		$u_key = implode('.', $u_names);
		$l_param_names[] = $u_key;
		$l_param_values[] = $t_names[0] == '' ? $t_value : rtl_sqlc_escape($t_value);
	}
	return sprintf('(%s) VALUES (%s)', implode(',', $l_param_names), implode(',', $l_param_values));
}

function rtl_sqlc_insert_values_multi($u_data_batch){

	if(!is_array($u_data_batch) || count($u_data_batch) == 0){
		return ''; //Не фалсе, а пустую строчку. Ибо так надо
	}

	$l_param_names = array();
	$l_param_values = array();

	$l_param_keys = array_keys(rtl_array_safe_test($u_data_batch[0]));

	foreach($l_param_keys as $k_name){
		$t_names = explode('.', $k_name);
		$u_names = array();

		foreach($t_names as $t_piece){
			$t_piece != '' && $u_names[] = "`$t_piece`";
		}
		$l_param_names[] = implode('.', $u_names);
	}
	foreach($u_data_batch as $t_row){
		$i_field = 0;
		$t_row_values = array();

		foreach(is_array($t_row) ? $t_row : rtl_array_safe_test($t_row) as $k_index => $t_value){
			is_numeric($k_index) && $k_index = $l_param_keys[$i_field];
			$t_row_values[] = $k_index[0] == '.' ? $t_value : rtl_sqlc_escape($t_value);

			++$i_field;
		}

		$l_param_values[] = '('. implode(',', $t_row_values) . ')';
	}

	return '('. implode(',', $l_param_names) . ') VALUES ' . implode(',', $l_param_values);
}


function rtl_sqlc_order_by($u_order = FALSE){
	if(is_string($u_order)){
		return $u_order;
	}
	$u_result = array();
	foreach($u_order as $k_field => $t_desc){
		$u_result[] = "$k_field ".($t_desc ? 'DESC' : 'ASC');
	}
	return implode(',', $u_result);
}

function rtl_bits_extract($n_source = 0){
	
	$r_bits = array();
	for($i_bit = 0; $i_bit < 32; ++$i_bit){
		($t_shift = 1 << $i_bit) & $n_source && $r_bits[] = $t_shift;
	}
	return $r_bits;
}


function rtl_sqlc_search_query($u_fields, $u_keyword, $is_case_sensitive = FALSE, $is_low_level = FALSE){

	$r_query_parts = array();
	$u_keyword = addslashes($u_keyword);
	!$is_low_level && $u_keyword = '%' . strtr($u_keyword, array(
		'%' => "\x5C%",
		'_' => "\x5C_"
	)) . '%';
	foreach($u_fields as $t_field){
		$r_query_parts[] = "$t_field LIKE '$u_keyword' COLLATE " . ($is_case_sensitive ? 'utf8_bin' : 'utf8_general_ci');
	}
	return '(' . implode(' || ', $r_query_parts) . ')';
}



/**
 * Преобразовывает обьект в массив, но оставляет нулл нуллом
 */
function rtl_array_safe_convert($o_source){
	if($o_source && is_object($o_source)){
		return (array)$o_source;
	}else{
		return NULL;
	}
}


function rtl_array_safe_test($o_source){
	return is_array($o_source) ? $o_source : array();
}


/**
 * получает массив из обьекта. ладно бы там, но делает это рекурсивно..
 * вот же до чего техника дошла! (с) применение в основном только для json_decode
 */
function rtl_array_from_object($u_object_source){

	if(!is_object($u_object_source) && !is_array($u_object_source)){
		return $u_object_source;
	}

	//хихи, как все просто то, писсец!
	$l_array_converted = (array)$u_object_source;

	foreach($l_array_converted as &$t_cell){
		//если полученный массив содержит в себе обьекты, их тоже передеываем в массивы
		is_object($t_cell) && $t_cell = rtl_array_from_object($t_cell);
	}

	//а возвращаем как вы уже догадались переделанный массив!
	return $l_array_converted;
}

/**
 * запрос к ебазеданных, и возврат результата в виде одного обьекта, тоесть только первого резулььтата выборки
 * функция все далет сама без посторонних помощщщей
 */
function rtl_database_query_object($u_query_sql, $is_fail_on_away = FALSE){
	CKernel::$db_link || rtl_database_connect();
	if(!CKernel::$db_link){
		return FALSE;
	}
	$r_fetched_data = FALSE;
	if($l_query_result = mysqli_query(CKernel::$db_link, $u_query_sql)){
		$r_fetched_data = mysqli_num_rows($l_query_result) > 0 ? mysqli_fetch_object($l_query_result) : FALSE;
		mysqli_free_result($l_query_result);
	}else{
		$t_errno = mysqli_errno(CKernel::$db_link);
		//mysql server has gone away, пытаемся что-нибудь с этим сделать
		if(2006 == $t_errno && !$is_fail_on_away){
			mysqli_close(CKernel::$db_link);
			if(rtl_database_connect()){
				return rtl_database_query_object($u_query_sql, TRUE);
			}
		}
		
		echo 
		"<br /><b style=\"color: #ff0000\">MySQL: //////////////////////////////////////////////////////////////////////<br />" .
		"====> [$t_errno] " . mysqli_error(CKernel::$db_link) . "<br />" .
		"====> " .  $u_query_sql . "<br /></b>";
		
		
	}
	return $r_fetched_data;
}

function rtl_database_query_array($u_query_sql, $is_fail_on_away = FALSE){

	CKernel::$db_link || rtl_database_connect();

	if(!CKernel::$db_link){
		return FALSE;
	}

	$r_fetched_data = FALSE;

	if($l_query_result = mysqli_query(CKernel::$db_link, $u_query_sql)){

		$r_fetched_data = array();

		if(mysqli_num_rows($l_query_result) > 0){
			while($t_fetched_object = mysqli_fetch_object($l_query_result)){
				$r_fetched_data[] = $t_fetched_object;
			}
		}

		mysqli_free_result($l_query_result);
	}else{

		$t_errno = mysqli_errno(CKernel::$db_link);

		//mysql server has gone away, пытаемся что-нибудь с этим сделать
		if(2006 == $t_errno && !$is_fail_on_away){
			mysqli_close(CKernel::$db_link);
			if(rtl_database_connect()){
				return rtl_database_query_array($u_query_sql, TRUE);
			}
		}
		
		echo 
		"<br /><b style=\"color: #ff0000\">MySQL: //////////////////////////////////////////////////////////////////////<br />" .
		"====> [$t_errno] " . mysqli_error(CKernel::$db_link) . "<br />" .
		"====> " .  $u_query_sql . "<br /></b>";
	}
	return $r_fetched_data;
}

function rtl_database_query_value($u_query_sql, $is_fail_on_away = FALSE){
	CKernel::$db_link || rtl_database_connect();

	if(!CKernel::$db_link){
		return FALSE;
	}

	$r_fetched_data = FALSE;
	//выполняем запрос
	if($l_query_result = mysqli_query(CKernel::$db_link, $u_query_sql)){

		if(mysqli_num_rows($l_query_result) > 0){
			$t = mysqli_fetch_row($l_query_result);
			$r_fetched_data = $t[0];
		}else{
			$r_fetched_data = FALSE;
		}

		mysqli_free_result($l_query_result);
	}else{
		$t_errno = mysqli_errno(CKernel::$db_link);

		if(2006 == $t_errno && !$is_fail_on_away){
			mysqli_close(CKernel::$db_link);
			if(rtl_database_connect()){
				return rtl_database_query_value($u_query_sql, TRUE);
			}
		}
		
		echo 
		"<br /><b style=\"color: #ff0000\">MySQL: //////////////////////////////////////////////////////////////////////<br />" .
		"====> [$t_errno] " . mysqli_error(CKernel::$db_link) . "<br />" .
		"====> " .  $u_query_sql . "<br /></b>";
	}
	return $r_fetched_data;
}

function rtl_database_query($u_query_sql, $is_fail_on_away = FALSE){
	CKernel::$db_link || rtl_database_connect();
	if(!CKernel::$db_link){
		return FALSE;
	}
	$r_fetched_data = FALSE;
	if($l_query_result = mysqli_query(CKernel::$db_link, $u_query_sql)){
		$r_fetched_data = $l_query_result;
	}else{
		$t_errno = mysqli_errno(CKernel::$db_link);
		if(2006 == $t_errno && !$is_fail_on_away){
			mysqli_close(CKernel::$db_link);
			if(rtl_database_connect()){
				return rtl_database_query($u_query_sql, TRUE);
			}
		}
		
		echo 
		"<br /><b style=\"color: #ff0000\">MySQL: //////////////////////////////////////////////////////////////////////<br />" .
		"====> [$t_errno] " . mysqli_error(CKernel::$db_link) . "<br />" .
		"====> " .  $u_query_sql . "<br /></b>";
	}
	return $r_fetched_data;
}

function rtl_database_insert($u_table_name, $u_data_insert, $u_flags = 0, $u_data_on_duplicate_key = FALSE, &$uv_rows_affected = 0){

	$l_query = sprintf(
		'%s %s INTO `%s` %s%s;',
		($u_flags & O_INSERT_REPLACE) != 0 ? 'REPLACE' : 'INSERT',
		(($u_flags & O_INSERT_DELAYED) != 0 ? 'DELAYED' : '') . (($u_flags & O_INSERT_IGNORE) != 0 ? 'IGNORE' : ''),
		$u_table_name,
		($u_flags & O_INSERT_MULTI) ? rtl_sqlc_insert_values_multi($u_data_insert) : rtl_sqlc_insert_values($u_data_insert),
		$u_data_on_duplicate_key ? ' ON DUPLICATE KEY UPDATE '.rtl_sqlc_update($u_data_on_duplicate_key) : ''
	);

	if($u_flags & O_INSERT_GET_RAW_SQL){
		return $l_query;
	}

	if(rtl_database_query($l_query)){
		$uv_rows_affected = mysqli_affected_rows(CKernel::$db_link);
		return mysqli_insert_id(CKernel::$db_link);
	}

	return FALSE;
}

function rtl_database_select(
	$u_fields_select = FALSE,
	$u_table_from = FALSE,
	$u_selector_join = FALSE,
	$u_selector_where = FALSE,
	$u_selector_order = FALSE,
	$u_fields_group = FALSE,
	$u_limit_start = FALSE,
	$u_limit_count = FALSE,
	&$uv_found_rows = 0,
	$u_flags = 0
){
	if(!$u_table_from || !$u_fields_select){
		return FALSE;
	}

	is_array($u_fields_select) && $u_fields_select = rtl_sqlc_select_fields($u_fields_select);
	is_array($u_table_from) && $u_table_from = implode(',', $u_table_from);
	is_array($u_fields_group) && $u_fields_group = implode(',', $u_fields_group);

	is_array($u_selector_where) && $u_selector_where = implode(' && ', $u_selector_where);
	$u_selector_join || $u_selector_join = array();

	$r_sql = "SELECT ";

	if($u_flags & O_SELECT_CALC_FOUND_ROWS){
		$r_sql .= 'SQL_CALC_FOUND_ROWS ';
	}

	if($u_flags & O_SELECT_FORCE_CACHE){
		$r_sql .= 'SQL_CACHE ';
	}


	if($u_flags & O_SELECT_FORCE_NOCACHE){
		$r_sql .= 'SQL_NO_CACHE ';
	}

	$r_sql .= "$u_fields_select FROM $u_table_from ";

	//дальше идут джоины... все сразу и опптом
	$r_sql .= rtl_sqlc_join($u_selector_join) . ' ';

	//дальше идут вхере, причем если там пустота, то они не идут
	$u_selector_where && $r_sql .= "WHERE $u_selector_where ";

	//дальше групбай
	$u_fields_group && $r_sql .= "GROUP BY $u_fields_group ";

	//дальше знаменитый ордербай
	$u_selector_order && $r_sql .= sprintf('ORDER BY %s ', rtl_sqlc_order_by($u_selector_order));

	if($u_limit_count){

		$r_sql .= 'LIMIT ';
		if($u_limit_start){
			$r_sql .= "$u_limit_start,$u_limit_count";
		}else{
			//такие лимиты тоже нужно поддерживать :)
			$r_sql .= $u_limit_count;
		}
	}

	if($u_flags & O_SELECT_GET_RAW_SQL){
		//Как все тупо и незамысловато, офигейц
		return $r_sql;
	}

	//завершаем запрос, чтобы он был очень красивым
	$r_sql .= ';';

	$uv_found_rows = FALSE;

	if($u_flags & O_SELECT_FIRST_ROW){
		//Для первой строки известный алгоритм
		$r_query_data = rtl_database_query_object($r_sql);
	}elseif($u_flags & O_SELECT_ONE_VALUE){
		//Это для всяких там каунтов и проч. хуйни
		$r_query_data = rtl_database_query_value($r_sql);
	}else{
		//А это обычный, ничем не примечательный запросик
		$r_query_data = rtl_database_query_array($r_sql);
	}

	if($u_flags & O_SELECT_CALC_FOUND_ROWS){
		$uv_found_rows = (int)rtl_database_query_value("SELECT FOUND_ROWS();");
	}

	return $r_query_data;
}

function rtl_database_update($u_table_name, $u_data_update, $u_selector = FALSE, $is_low_priority = FALSE){

	($l_where = implode(' && ', $u_selector)) || $l_where = 1;
	$l_query = sprintf('UPDATE %s %s SET %s WHERE %s;', $is_low_priority ? 'LOW_PRIORITY' : '', $u_table_name, rtl_sqlc_update($u_data_update), $l_where);

	if(rtl_database_query($l_query)){
		return mysqli_affected_rows(CKernel::$db_link);
	}

	return FALSE;
}

function rtl_init(){
	rtl_config_parse();
}

function rtl_get_last_error(){
	if($_SESSION['error_message']){
		$k = $_SESSION['error_message'];
		unset($_SESSION['error_message']);
		return $k;
	}else{
		return NULL;
	}
}

function rtl_array_reindex($u_array_source, $u_field_key = 'id', $u_field_value = FALSE, $is_numeric_index = TRUE, $is_mdi = FALSE){
	$r_array_result = array();

	//берем исходный массив
	foreach($u_array_source as &$t_row){

		//извлекаем значение индекса находу определяя из чего
		$u_field_key && $t_index_value = is_object($t_row) ? $t_row->$u_field_key : $t_row[$u_field_key];

		$t_row_value = $u_field_value === FALSE ? $t_row : (is_object($t_row) ? $t_row->$u_field_value : $t_row[$u_field_value]);

		//вставляем в результат, если сказано одно поле - вставляем одно, а если не сказано - вставляем все...
		if($u_field_key){
			//Вот такое дополнение, позволяющее существовать неиндексированым массивам...
			$k = $is_numeric_index ? intval($t_index_value) : strval($t_index_value);

			if($is_mdi){
				$r_array_result[$k][] = $t_row_value;
			}else{
				$r_array_result[$k] = $t_row_value;
			}
		}else{
			$r_array_result[] = $t_row_value;
		}

	}

	//делаем профит
	return $r_array_result;
}

function rtl_load_pairs_set($table_name, $column_name){
	return rtl_array_reindex(rtl_database_select(array(
		'id', 
		$column_name
	), $table_name, FALSE, FALSE, FALSE, FALSE, 0, 0, $found_rows, O_SELECT_FORCE_CACHE), 'id', $column_name, TRUE, FALSE);
}


function rtl_get_merge_parameters($psave, $pextend = array(), $is_compose_url = FALSE){
	$r = array();
	
	foreach($psave as $wort){
		if(isset($_GET[$wort])){
			$r[$wort] = $_GET[$wort];
		}
	}
	
	$r = array_merge($r, $pextend);
		
	if($is_compose_url){
		
		$uriy = array();
		
		foreach($r as $k => $v){
			$uriy[] = $k . '=' . rawurlencode($v);
		}
		
		return implode('&', $uriy);
		
	}else{
		return $r;
	}
}


function rtl_format($u_string_source, $u_values, $u_separator_left = '{', $u_separator_right = '}', $is_remove_unused_templates = FALSE){

	//тут будут храниться готовая таблица замены
	$l_replace_hash = array();

	foreach($u_values as $k_alias => $t_value){
		//во бля, ты сам то это читал?*
		$l_replace_hash["$u_separator_left$k_alias$u_separator_right"] = $t_value;
	}

	$l_result = count($l_replace_hash) > 0 ? strtr($u_string_source, $l_replace_hash) : $u_string_source;

	//эта строчка реализует выпил ненужных шаблонов, если надо :)
	return $is_remove_unused_templates ? preg_replace(sprintf('/%s/', preg_quote($u_separator_left).'[\w\.\-\:]*'.preg_quote($u_separator_right)), '', $l_result) : $l_result;
}


function rtl_image_make_auto_thumbnail(
	$u_image_file,
	$u_max_width,
	$u_max_height,
	$is_crop_algo = FALSE,
	$is_allow_backward_scale = FALSE,
	$is_keep_requested_size = FALSE,
	$u_background_color = 0,
	$u_cache_dir = '/auto-thumb'
){

	//сначала пытаемся найти картинку в "кеше"
	$u_cache_filename = sprintf('%s.%d.%d.%s-%s-%s.auto-thumb.jpg', md5($u_image_file.$u_max_width.$u_max_height.$u_background_color.$u_cache_dir.$u_image_file), $u_max_width, $u_max_height, $is_crop_algo ? 'crop' : 'adjust', $is_allow_backward_scale ? 'back' : 'fwd', $is_keep_requested_size ? 'krqs' : 'ondemand');

	//если уже есть в кеше - то возвращаем на нее ссылку. повторно не переделываем ее даже если нас будут резать
	if(!is_file($u_cache_path_full = "$_SERVER[DOCUMENT_ROOT]$u_cache_dir/$u_cache_filename")){

		//если фотку прочитали
		if(($l_image_sizes = getimagesize("$_SERVER[DOCUMENT_ROOT]$u_image_file")) && $l_image_sizes[0] && $l_image_sizes[1]){

			//только если есть необходимость пережимать картинку
			if($l_image_sizes[0] > $u_max_width || $l_image_sizes[1] > $u_max_height || $is_keep_requested_size){

				if($is_crop_algo){

					$n_koeff = abs($t_kh = $l_image_sizes[1] / $u_max_height) < abs($t_kw = $l_image_sizes[0] / $u_max_width) ? $t_kh : $t_kw;

					$n_result_src_w = $n_koeff * $u_max_width;
					$n_result_src_h = $n_koeff * $u_max_width;

					//если не сказано сохранять размер, то квадратик мы ужимаем до максимаьно возможного
					$n_src_w = $is_allow_backward_scale ? $n_result_src_w : max($n_result_src_w, $u_max_width);
					$n_src_h = $is_allow_backward_scale ? $n_result_src_h : max($n_result_src_h, $u_max_height);

					$n_src_x = round(($l_image_sizes[0] / 2) - ($n_src_w / 2));
					$n_src_y = round(($l_image_sizes[1] / 2) - ($n_src_h / 2));

					//таки берем, и рисуем
					if($u_image_source = @imagecreatefromstring(@file_get_contents("$_SERVER[DOCUMENT_ROOT]$u_image_file"))){

						$u_image_destination = imagecreatetruecolor($u_max_width, $u_max_height);

						imagecopyresampled($u_image_destination, $u_image_source, 0, 0, $n_src_x, $n_src_y, $u_max_width, $u_max_height, $n_src_w, $n_src_h);
						imagejpeg($u_image_destination, $u_cache_path_full, 100);

						//выносим мусор,сука
						imagedestroy($u_image_destination);
						imagedestroy($u_image_source);
					}

				}else{
					$n_koeff = abs($t_kh = $l_image_sizes[1] / $u_max_height) > abs($t_kw = $l_image_sizes[0] / $u_max_width) ? $t_kh : $t_kw;

					//результат считается несколько инакче чем выше
					$n_result_dst_w = $l_image_sizes[0] / $n_koeff;
					$n_result_dst_h = $l_image_sizes[1] / $n_koeff;

					$n_dst_w = $n_result_dst_w;
					$n_dst_h = $n_result_dst_h;

					//рисуем
					if($u_image_source = @imagecreatefromstring(@file_get_contents("$_SERVER[DOCUMENT_ROOT]$u_image_file"))){

						$n_dst_image_w = $is_keep_requested_size ? $u_max_width : $n_dst_w;
						$n_dst_image_h = $is_keep_requested_size ? $u_max_height : $n_dst_h;
						$u_image_destination = imagecreatetruecolor($n_dst_image_w, $n_dst_image_h);

						//миздец, я не уверен что это будет работать прайвельно
						imagecopyresampled($u_image_destination, $u_image_source, 0, 0, 0, 0, $n_dst_image_w, $n_dst_image_h, $l_image_sizes[0], $l_image_sizes[1]);
						imagejpeg($u_image_destination, $u_cache_path_full, 100);

						//выносим мусор,сука
						imagedestroy($u_image_destination);
						imagedestroy($u_image_source);
					}
				}
			}else{
				//просто копируем на место тумбы оригинальную картинку
				return $u_image_file;
			}

		}else{
			return FALSE; //imagenotreadable.png
		}

	}

	//ну мы типа малаццы
	return "$u_cache_dir/$u_cache_filename";
}


function rtl_string_translit($u_string){
	return preg_replace('/\W/is', '_', strtr(trim($u_string), array(
		'а' => 'a',
		'б' => 'b',
		'в' => 'v',
		'г' => 'g',
		'д' => 'd',
		'е' => 'e',
		'ё' => 'eo',
		'ж' => 'zh',
		'з' => 'z',
		'и' => 'i',
		'й' => 'iy',
		'к' => 'k',
		'л' => 'l',
		'м' => 'm',
		'н' => 'n',
		'о' => 'o',
		'п' => 'p',
		'р' => 'r',
		'с' => 's',
		'т' => 't',
		'у' => 'u',
		'ф' => 'f',
		'х' => 'h',
		'ц' => 'c',
		'ч' => 'ch',
		'ш' => 'sh',
		'щ' => 'sch',
		'ъ' => 'jy',
		'ы' => 'y',
		'ь' => 'j',
		'э' => 'je',
		'ю' => 'ju',
		'я' => 'ja',
		'А' => 'A',
		'Б' => 'B',
		'В' => 'V',
		'Г' => 'G',
		'Д' => 'D',
		'Е' => 'E',
		'Ё' => 'EO',
		'Ж' => 'ZH',
		'З' => 'Z',
		'И' => 'I',
		'Й' => 'IY',
		'К' => 'K',
		'Л' => 'L',
		'М' => 'M',
		'Н' => 'N',
		'О' => 'O',
		'П' => 'P',
		'Р' => 'R',
		'С' => 'S',
		'Т' => 'T',
		'У' => 'U',
		'Ф' => 'F',
		'Х' => 'H',
		'Ц' => 'C',
		'Ч' => 'CH',
		'Ш' => 'SH',
		'Щ' => 'SCH',
		'Ъ' => 'JY',
		'Ы' => 'Y',
		'Ь' => 'J',
		'Э' => 'JE',
		'Ю' => 'JU',
		'Я' => 'JA'
	)));
}


function rtl_files_upload($u_input_files_list, $u_directory, $u_allowed_ext = NULL, $u_max_size = 409600){

	//ну это защита от дебилов
	if(!count($u_input_files_list)){
		return array();
	}

	//это чтобы можно было и просто имена передавать :)
	is_array($u_input_files_list) || $u_input_files_list = array($u_input_files_list);

	$u_allowed_ext || $u_allowed_ext = array('jpg', 'jpeg', 'png');

	$u_result_files = array();

	$n_files_count = is_array($u_input_files_list['name']) ? count($u_input_files_list['name']) : 1;

	for($i_file_index = 0; $i_file_index < $n_files_count; $i_file_index++){

		$t_file = is_array($u_input_files_list['name']) ? array(
			'name' => $u_input_files_list['name'][$i_file_index],
			'type' => $u_input_files_list['type'][$i_file_index],
			'tmp_name' => $u_input_files_list['tmp_name'][$i_file_index],
			'error' => $u_input_files_list['error'][$i_file_index],
			'size' => $u_input_files_list['size'][$i_file_index]
		) : $u_input_files_list;

		//выуживаем расширение файла
		$l_file_ext = rtl_string_translit(array_pop(explode('.', $t_file['name'])));
		
		//ну это понятно
		if(!$t_file['error'] && $l_new_file_name = substr(rtl_string_translit(preg_replace('/\..*$/is', '', $t_file['name'])), 0, 100)){
			
			if($t_file['size'] <= $u_max_size){

				if(in_array($l_file_ext, $u_allowed_ext)){
					$u_file_timestamp = date('Y-m-d.H-i-s.').md5(microtime().mt_rand(1, 0x7ffffffa));

					$u_result_file_name = "$u_directory/$u_file_timestamp.$l_new_file_name.$l_file_ext";

					$filupka = "$_SERVER[DOCUMENT_ROOT]$u_result_file_name";

					if(is_file($filupka)) unlink($filupka);

					//вытаскиваем файлик
					if(move_uploaded_file($t_file['tmp_name'], $filupka)){

						//добавляем его в результирующий массив
						$u_result_files[] = array(
							//'name-original' => $t_file['name'],
							'filename' => $u_result_file_name,
							'filesize' => $t_file['size'],
							//'file-content-type' => $t_file['type']
						);
					}
				}else{
					CKernel::KeRegisterMessage("Ошибка загрузки файла (недопустимое расширение)", CKernel::ERROR_LEVEL_WARNING);
				}
			}else{
				CKernel::KeRegisterMessage("Ошибка загрузки файла (превышен размер 400 Кб)", CKernel::ERROR_LEVEL_WARNING);
			}
		}else{
			if(4 !==(int) $t_file['error']){
				CKernel::KeRegisterMessage("Ошибка загрузки файла ($t_file[error])", CKernel::ERROR_LEVEL_WARNING);
			}
		}

	}

	return $u_result_files;
}


function rtl_reply_headers($u_headers, $is_replace_existing = TRUE){

	foreach($u_headers as $k_header_name => $t_header){
		header(sprintf('%s: %s', $k_header_name, $t_header), $is_replace_existing);
	}

	return TRUE;
}


function rtl_reply_html($content, $http_response_code = 200){
	
	if(connection_status() == CONNECTION_NORMAL){

		header('Content-Type: text/html; charset=utf-8', TRUE, $http_response_code);
		
		rtl_reply_headers(CKernel::$config['core-default-http-headers']);
		
		echo $content;
	}
	
	exit;
}

function rtl_local_redirect($rui, $http_response_code_code = 302){
	if(!preg_match('@^/[/\w\%\&\=\?#]+$@', $rui)) $rui = '/';
	header("Location: $rui", TRUE, $http_response_code_code);
}

function arshloch($x){for($i=0;$i<117;++$i)$x=md5(sha1($x, TRUE));return$x;}


/**
 * Взято из FDO.Interface, рисует красивый временной интервал
 * @return string the readable date iterval
 */

function rtl_utils_date_interval($d = NULL, $s_delim = ':', $hs_tpl = NULL, $n_flags = NULL){
	$d === NULL && $d = time();
	$n_flags === NULL && $n_flags = O_DINT_SECOND | O_DINT_MINUTE | O_DINT_HOUR;

	$zsec = $d % 60;
	$zmin = (floor($d/60)) % 60;
	$zhur = (floor($d/3600)) % 24;
	$zday = (floor($d/86400)) % 365;
	$zyar = (floor($d/31536000));

	$rzlt = array();

	if(!$hs_tpl){
		$hs_tpl = array('%02d', '%02d', '%02d', '%02d', '%02d');
	}

	if((O_DINT_YEAR & $n_flags) && $hs_tpl[0] && ($zyar > 0 || (O_DALW_YEAR & $n_flags))){
		$rzlt[] = sprintf($hs_tpl[0], $zyar);
	}

	if((O_DINT_DAY & $n_flags) && $hs_tpl[1] && ($zyar > 0 || $zday > 0 || (O_DALW_DAY & $n_flags))){
		$rzlt[] = sprintf($hs_tpl[1], $zday);
	}

	if((O_DINT_HOUR & $n_flags) && $hs_tpl[2] && ($zyar > 0 || $zday > 0 || $zhur > 0 || (O_DALW_HOUR & $n_flags))){
		$rzlt[] = sprintf($hs_tpl[2], $zhur);
	}

	if((O_DINT_MINUTE & $n_flags) && $hs_tpl[3] && ($zyar > 0 || $zday > 0 || $zhur > 0 || $zmin > 0 || (O_DALW_MINUTE & $n_flags))){
		$rzlt[] = sprintf($hs_tpl[3], $zmin);
	}

	if((O_DINT_SECOND & $n_flags) && $hs_tpl[4] && ($zyar > 0 || $zday > 0 || $zhur > 0 || $zmin > 0 || $zsec > 0 || (O_DALW_SECOND & $n_flags))){
		$rzlt[] = sprintf($hs_tpl[4], $zsec);
	}

	$rx = implode($s_delim, $rzlt);

	return $rx != '' ? $rx : 'мгновение';
}
