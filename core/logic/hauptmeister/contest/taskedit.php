<?

lib_load('meister.php');
lib_load('users.php');
lib_load('contest.php');

$id_task = 0;

if(count(CKernel::$request) > 0){
	$id_task = (int) array_shift(CKernel::$request);
}

$wata = [
	'is_active' => 1
];

if($_POST['aufstellen_task']){
	
	$die_fielde = [
		'name_task',
		'tflag',
		'descr',
	];

	foreach($die_fielde as $zeile){
		$wata[$zeile] = $_POST[$zeile];
	}
	
	$wata['is_active'] = !!$_POST['is_active'];
	$wata['is_removed'] = !!$_POST['is_removed'];
	
	
	if($id_task){

		if(rtl_database_update('ctf_tasks', $wata, [
			"id=$id_task"
		])){
			CKernel::KeRegisterMessage('Таска обновлена', CKernel::ERROR_LEVEL_INFO);
		}else{
			CKernel::KeRegisterMessage('Что-то не пошло', CKernel::ERROR_LEVEL_WARNING);
		}

	}else{
		
		if($id_task = rtl_database_insert('ctf_tasks', $wata)){
			CKernel::KeRegisterMessage('Таска создана', CKernel::ERROR_LEVEL_INFO);
			CKernel::KeSaveMessages();
			rtl_local_redirect("/hauptmeister/contest/taskedit/$id_task", 302);
			exit;
		}else{
			CKernel::KeRegisterMessage('Что-то не пошло', CKernel::ERROR_LEVEL_WARNING);
		}
	}
}

$die_task = $id_task ? CLibContest::LoadTaskAllein($id_task) : (object) $wata;

CKernel::KeRestoreMessages();

rtl_reply_html(tpl_load('layout/layout_hauptmeister.php', array(
	'title' => 'CTF :: Contest :: ' . ($id_task ? "Редактируем таску №$id_task" : "Создаём таску"),
	'content' => tpl_load('pages/' . implode('/', CKernel::$reqpath) . '.php', array(
		'die_task' => $die_task,
		'messages' => CKernel::KeGetMessages()
	))
)));