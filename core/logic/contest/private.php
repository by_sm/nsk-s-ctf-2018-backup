<?

//var_dump($_SERVER);
//exit;

function CheckHTTPS(){
	return 'https' === $_SERVER['HTTP_X_FORWARDED_PROTO'];
}

if(!checkHTTPS()){
	// HTTPS only
	
	if(preg_match('![\r\n]!', $_SERVER['REQUEST_URI'])){
		die("ERROR");
	}
	
	header("Location: https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]", TRUE, 301);
	exit;
}


if($_POST['form_login']){
	$lg = trim($_POST['lg']);
	if(preg_match(RTL_REGEXP_VARNAME, $lg)){
		
		if(CLibUser::AuthenticateCProfile($lg, $_POST['pw'])){
			rtl_local_redirect($_SERVER['REQUEST_URI'], 302);
		}
	}else{
		CKernel::KeRegisterMessage("Некорректный логин", CKernel::ERROR_LEVEL_WARNING);
	}
}


if(CLibUser::LoadCProfile()){
	CKernel::KeContinue('tasks');
	exit;
}else{
	rtl_reply_html(tpl_load('layout/layout_contest.php', array(
		'title' => 'Авторизация',
		'content' => tpl_load('pages/' . implode('/', CKernel::$reqpath) . '.php', array(
			/*'die_task' => $die_task,*/
			'messages' => CKernel::KeGetMessages()
		))
	)));
}