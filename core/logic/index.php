<?php

lib_load('epages.php');
lib_load('kompiler.php');

$epage = CLibEpages::LoadPage('index');


if($epage->is_disabled){
	
	rtl_reply_html(tpl_load('layout/layout_main.php', array(
		'title' => $epage->title,
		'content' => tpl_load('pages/page-disabled.php', [])
	)));
	
}else{
	$epota = CLibEpages::LoadPosts((int) $epage->id);
	
	rtl_reply_html(tpl_load('layout/layout_main.php', array(
		'title' => $epage->title,
		'content' => tpl_load('pages/' . implode('/', CKernel::$reqpath) . '.php', array(
			'epage' => $epage,
			'epota' => $epota,
			'econtent' => CLibKompiler::KompileTemplate($epage->etext, [
				'posts' => $epota
			])
		))
	)));
}