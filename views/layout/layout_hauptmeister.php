<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru" dir="ltr" xmlns:og="http://ogp.me/ns#" >
<head>
	<!-- Created by FadeDEAD -->
	<title><?=$title?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<!-- favicon -->
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<!-- opengraph -->
	<meta property="og:locale" content="ru_RU" />
	<meta name="language" content="ru-RU" />
	<meta name="resource-type" content="document" />
	<!-- links -->
	<!-- css -->
	<link rel="stylesheet" type="text/css" href="/css/style-meister.css" />

	<!-- external javascript -->
	<script type="text/javascript" src="/js/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="/js/tinymce/tinymce.min.js"></script>
	<script type="text/javascript" src="/js/runtime.js"></script>
	<script type="text/javascript" src="/js/meister-interface.js"></script>
</head>
<body>
<div class="h-container">
	<h2><?=$title?></h2>

	<div class="h-menuet">
		<a href="/hauptmeister/hauptindex">Главная</a> |
		<a href="/hauptmeister/hauptteams">Команды</a> |
		<a href="/hauptmeister/hauptposts">Посты</a> |
		<a href="/hauptmeister/contest">Контесты</a> |
		<a href="/hauptmeister/hauptpasswort">Пароль</a> |
		<a href="/" target="_blank">Открыть сайт</a>
	</div>

	<div class="h-content">
	<?=$content?>
	</div>
</div>
</body>
</html>
