<?=tpl_load('print-gemeister.php', array(
	'messages' => $messages
))?>
<form action="" method="post" enctype="multipart/form-data">
	<div>
		<table class="h-forme">
			<tr>
				<td>ФИО капитана:</td>
				<td><input type="text" name="fio_captain" value="<?=htmlspecialchars(trim($das_team->fio_captain))?>" /></td>
			</tr>
			<tr>
				<td>ФИО участников:</td>
				<td>
					<textarea name="fio_alles"><?=htmlspecialchars(trim($das_team->fio_alles))?></textarea>
				</td>
			</tr>
			<tr>
				<td>Название команды:</td>
				<td><input type="text" name="name_team" value="<?=htmlspecialchars(trim($das_team->name_team))?>" /></td>
			</tr>
			<tr>
				<td>Логотип:</td>
				<td>
					<? if($das_team->logo_path){?>
					<div id="teamform-logotype-loaded"><img src="<?=$das_team->logo_path?>" /></div>
					<div id="teamform-logotype-linkot"><a href="javascript:;">убрать этот логотип</a></div>
					<div style="display: none" id="teamform-logotype-formen"><input type="file" name="logo_path" /></div>
					<input type="hidden" name="logo_kill" value="0" />
					<? }else{?>
					<div id="teamform-logotype-formen"><input type="file" name="logo_path" /></div>
					<? }?>
				</td>
			</tr>
			<tr>
				<td>Школа/университет:</td>
				<td><input type="text" name="school" value="<?=htmlspecialchars(trim($das_team->school))?>" /></td>
			</tr>
			<tr>
				<td>Курс/класс:</td>
				<td><input type="text" name="klasse" value="<?=htmlspecialchars(trim($das_team->klasse))?>" /></td>
			</tr>
			<tr>
				<td>Контактная информация:</td>
				<td>
					<textarea name="contacts"><?=htmlspecialchars(trim($das_team->contacts))?></textarea>
				</td>
			</tr>
			<tr>
				<td>Не школьники:</td>
				<td><input type="checkbox" name="typeindex" value="eins" <?=$das_team->typeindex ? 'checked="checked"' : ''?> /></td>
			</tr>
			<tr>
				<td>Команда удалена:</td>
				<td><input type="checkbox" name="is_removed" value="eins" <?=$das_team->is_removed ? 'checked="checked"' : ''?> /></td>
			</tr>
			<tr>
				<td>Дата(регистрации):</td>
				<td><input type="text" name="date_reg" value="<?=htmlspecialchars(trim($das_team->date_reg))?>" /></td>
			</tr>
			<tr>
				<td><input type="submit" name='send_registration' value="Aufstellen" /></td>
			</tr>
		</table>
	</div>
</form>
<?=tpl_load('print-gemeister.php', array(
	'messages' => $messages
))?>