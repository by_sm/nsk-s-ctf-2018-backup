<div id="ctf-reg" class="overlay">
	<div class="popup">
		<a class="close" href="#">&times;</a>
		<div class="content">
			<div class="form">
				<h1><?=$tplvars['title']?></h1>
				<?=tpl_load('register-form.php', array(
					'der_zeiger' => !(int)$tplvars['school'],
					'fehler' => $defvars['fehler'],
					'namenschule' => $tplvars['namenschule'],
					'namenklasse' => $tplvars['namenklasse']
				))?>
			</div>
		</div>
	</div>
</div>
