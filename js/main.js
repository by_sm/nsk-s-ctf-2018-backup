$(function() {
	window.timestart = new Date().getTime() / 1000.0;
	
    var validate = function(el) {
        var parent = $(el).parents("div.pure-control-group");
		
		if($(el).is('input[type="file"]')) return false;
		if($(el).is('input[type="hidden"]')) return false;
		
        if ($(el).val() == "" || $(el).parent().find("input.upload-button").val() == "") {
            parent.addClass("error");
			return true;
        } else {
            parent.removeClass("error");
			return false;
        }
    };
    var validateAll = function() {
		var heiss = false;
        $("input, textarea, #remove, #upload").each(function(key, el) {
            heiss |= validate(el);
        });
		return heiss;
    };
	
    var wrapper = $( ".upload-container" ),
        inp = wrapper.find( "input.upload-button" ),
        btn = wrapper.find( "span.pure-button" ),
        lbl = wrapper.find( "span.upload-filename" );
    btn.focus(function(){
        inp.focus()
    });
// Crutches for the :focus style:
    inp.focus(function(){
        wrapper.addClass( "focus" );
    }).blur(function(){
        wrapper.removeClass( "focus" );
    });
    btn.add( lbl ).click(function(){
        inp.click();
    });
    btn.focus(function(){
        wrapper.addClass( "focus" );
    }).blur(function(){
        wrapper.removeClass( "focus" );
    });
    var file_api = ( window.File && window.FileReader && window.FileList && window.Blob ) ? true : false;

    inp.change(function(){
        var file_name;
        if( file_api && inp[ 0 ].files[ 0 ] )
            file_name = inp[ 0 ].files[ 0 ].name;
        else
            file_name = inp.val().replace( "C:\\fakepath\\", '' );

        if( ! file_name.length )
            return;

        if( lbl.is( ":visible" ) ){
            lbl.text( file_name );
            btn.text( "Загрузить" );
        }else
            btn.text( file_name );
    }).change();
    $( window ).resize(function(){
        $( ".file_upload input" ).triggerHandler( "change" );
    });
	
	$('#teamform-logotype-loaded a').click(function(ev){
		$('#teamform-logotype-loaded').remove();
		$('#teamform-logotype-formen').css({
			display: 'inline-block'
		});
		$('input[name="logo_kill"]').val(1);
	});
	
	$('#register').click(function(){
		if(!validateAll()){
			$(this).parents('form').first().submit();
		}
	});
	
	
	if($('.es-geht').length > 0 || $('.es-wird').length > 0){
	
		var lalka = function(){

			$('.es-geht').each(function(){
				var es_geht = parseInt($(this).attr('das:es-geht'));
				var correkt = (new Date().getTime() / 1000.0) - window.timestart;


				$(this).html(fd_showdate_interval(Math.max(es_geht - correkt, 0), ':', null, false));
			});


			$('.es-wird').each(function(){
				var es_wird = parseInt($(this).attr('das:es-wird'));
				var correkt = (new Date().getTime() / 1000.0) - window.timestart;


				$(this).html(fd_showdate_interval(Math.max(es_wird - correkt, 0), ':', null, false));
			});

			setTimeout(lalka, 250);
		};

		setTimeout(lalka, 250);
	}
	
});