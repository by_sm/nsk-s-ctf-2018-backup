<?

class CLibKompiler {

	private static $default_vars;

	public static function KompileTemplate($tpl, $defvars){

		self::$default_vars = $defvars;

		return preg_replace_callback('#\{\{(.+?)\}\}#uis', function($match){

			$dass = array_map('trim', explode('||', $match[1]));

			$vars = [];

			foreach($dass as $ass){

				$pair = array_map('trim', explode('::', $ass, 2));

				if(2 === count($pair) && is_string($pair[0]) && '' !== $pair[0]){
					$vars[$pair[0]] = $pair[1];
				}
			}
			
			if(is_string($vars['f']) && preg_match('#^[\w\-\_]+$#', $vars['f'])){
					
				$fuhle = 'komponente/' . $vars['f'] . '.php';
				
				if(is_file($_SERVER['DOCUMENT_ROOT'] . '/views/' . $fuhle)){
				
					return tpl_load($fuhle, [
						'tplvars' => $vars,
						'defvars' => CLibKompiler::$default_vars
					]);
				}else{
					return 'Invalid function: ' . $vars['f'];
				}
			}
			
			return '';

		}, preg_replace('#\{\{\*.+?\}\}#iuS', '', $tpl));
	}
	
}
