<?php

lib_load('meister.php');
lib_load('users.php');

$id_team = (int) array_shift(CKernel::$request);

if($_POST['send_registration']){

	$die_fielde = [
		'fio_captain',
		'fio_alles',
		'name_team',
		'school',
		'klasse',
		'contacts',
		'date_reg'
	];

	$wata = [];

	foreach($die_fielde as $zeile){
		$wata[$zeile] = $_POST[$zeile];
	}

	$wata['typeindex'] = !!$_POST['typeindex'];
	$wata['is_removed'] = !!$_POST['is_removed'];

	if(count($lopik = rtl_files_upload($_FILES['logo_path'], '/logotypes')) > 0){
		$wata['logo_path'] = $lopik[0]['filename'];
	}else if($_POST['logo_kill']){
		$wata['logo_path'] = '';
	}

	if($id_team){

		if(rtl_database_update('ctf_teams', $wata, [
			"id=$id_team"
		])){
			CKernel::KeRegisterMessage('Команда обновлена', CKernel::ERROR_LEVEL_INFO);
		}else{
			CKernel::KeRegisterMessage('Что-то не пошло', CKernel::ERROR_LEVEL_WARNING);
		}

	}else{
		if($id_team = rtl_database_insert('ctf_teams', $wata)){
			CKernel::KeRegisterMessage('Команда создана', CKernel::ERROR_LEVEL_INFO);
			CKernel::KeSaveMessages();
			rtl_local_redirect("/hauptmeister/haupteditteam/$id_team", 302);
			exit;
		}else{
			CKernel::KeRegisterMessage('Что-то не пошло', CKernel::ERROR_LEVEL_WARNING);
		}
	}
}

$das_team = $id_team ? CLibMeister::FindTeamById($id_team) : (object) ['date_reg' => date('Y-m-d H:i:s')];

CKernel::KeRestoreMessages();

rtl_reply_html(tpl_load('layout/layout_hauptmeister.php', array(
	'title' => 'CTF :: Meister :: ' . ($id_team ? "Редактируем команду №$id_team" : "Создаём команду"),
	'content' => tpl_load('pages/' . implode('/', CKernel::$reqpath) . '.php', array(
		'das_team' => $das_team,
		'messages' => CKernel::KeGetMessages()
	))
)));
