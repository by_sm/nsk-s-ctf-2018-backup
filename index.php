<?

error_reporting(E_ALL & ~(E_STRICT | E_NOTICE));
ini_set('display_errors', 0);

require_once "$_SERVER[DOCUMENT_ROOT]/core/kernel.php";

rtl_init();

CKernel::AnalyzeRequest();

CKernel::KeContinue('index');
