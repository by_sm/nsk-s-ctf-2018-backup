$(function(){
	window.timestart = new Date().getTime() / 1000.0;

	$('#teamform-logotype-linkot a').click(function(ev){
		$('#teamform-logotype-linkot, #teamform-logotype-loaded').remove();
		$('#teamform-logotype-formen').css({
			display: 'block'
		});
		$('input[name="logo_kill"]').val(1);
	});

	//$('textarea.tiny-mce').tinymce();
	tinymce.init({
		selector:'textarea.tiny-mce',
		plugins : "link,image,code,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template"
	});


	$('#n_points_plus').click(function(){
		var poni = parseInt(prompt('Сколько прибавить?', '0'));
		var pino = parseInt($('#n_points_offset').val());

		$('#n_points_offset').val(pino + poni);
	});

	$('#n_points_minus').click(function(){
		var poni = parseInt(prompt('Сколько отжать?', '0'));
		var pino = parseInt($('#n_points_offset').val());

		$('#n_points_offset').val(pino - poni);
	});

	if($('.es-geht').length > 0 || $('.es-wird').length > 0){

		var lalka = function(){

			$('.es-geht').each(function(){
				var es_geht = parseInt($(this).attr('das:es-geht'));
				var correkt = (new Date().getTime() / 1000.0) - window.timestart;


				$(this).html(fd_showdate_interval(Math.max(es_geht - correkt, 0), ':', null, false));
			});


			$('.es-wird').each(function(){
				var es_wird = parseInt($(this).attr('das:es-wird'));
				var correkt = (new Date().getTime() / 1000.0) - window.timestart;


				$(this).html(fd_showdate_interval(Math.max(es_wird - correkt, 0), ':', null, false));
			});

			setTimeout(lalka, 250);
		};

		setTimeout(lalka, 250);
	}

});
