<?=tpl_load('contest-menuet.php')?>
<?=tpl_load('print-gemeister.php', array(
	'messages' => $messages
))?>
<form action="" method="post">
	<div>
		<table class="h-forme">
			<tr>
				<td>Название:</td>
				<td><input type="text" name="name_contest" value="<?=htmlspecialchars(trim($das_kontest->name_contest))?>" /></td>
			</tr>
			<tr>
				<td>Продолжительность (минуты):</td>
				<td><input type="text" name="lange" value="<?=(int) trim($das_kontest->lange)?>" /></td>
			</tr>
			<tr>
				<td>Дата начала:</td>
				<td><input type="text" name="date_start" value="<?=htmlspecialchars(trim($das_kontest->date_start))?>" /></td>
			</tr>
			<tr>
				<td>Активность:</td>
				<td><input type="checkbox" name="is_active" value="eins" <?=$das_kontest->is_active ? 'checked="checked"' : ''?> /></td>
			</tr>
			<tr>
				<td>Удалён:</td>
				<td><input type="checkbox" name="is_removed" value="eins" <?=$das_kontest->is_removed ? 'checked="checked"' : ''?> /></td>
			</tr>
			<tr>
				<td><input type="submit" name="aufstellen_contest" value="Aufstellen" /></td>
			</tr>
		</table>
	</div>
</form>
<?=tpl_load('print-gemeister.php', array(
	'messages' => $messages
))?>