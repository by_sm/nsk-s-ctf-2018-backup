<?

if(count(CKernel::$request) < 1){
	echo "Ну бля!";
	exit;
}

$id_contest = (int) array_shift(CKernel::$request);


if(count(CKernel::$request) > 0){
	
	$id_cuser = (int) array_shift(CKernel::$request);
	
	$der_nutzer = CLibContest::LoadCUsersAllesDamit($id_cuser);
	$der_nutzer = array_shift($der_nutzer);
	
	
	$record = CLibContest::LoadCUserPointsOffsetRecord($id_contest, $id_cuser);
	
	if(!$record){
		$record = (object) [
			'id_contest' => $id_contest,
			'id_user' => $id_cuser,
			'points' => 0
		];
	}
	
	if($_POST['aufstellen_editrating']){
		$record->points = (int) $_POST['points'];
		if(CLibContest::ReplaceCUserPointsOffsetRecord($record)){
			CKernel::KeRegisterMessage('Alles gut!', CKernel::ERROR_LEVEL_INFO);
		}else{
			CKernel::KeRegisterMessage('Donnerwetter!', CKernel::ERROR_LEVEL_WARNING);
		}
	}
	
	
	
	rtl_reply_html(tpl_load('layout/layout_hauptmeister.php', array(
		'title' => "CTF :: Contest :: Редактируем поинты команде «{$der_nutzer->name_team}»",
		'content' => tpl_load('pages/' . implode('/', CKernel::$reqpath) . '-editrating.php', array(
			'messages' => CKernel::KeGetMessages(),
			'das_rekord' => $record
		))
	)));
	
}else{
	
	if($_POST['aufstellen_selectuser']){
		
		$id_cuser = $_POST['id_cuser'];
		
		rtl_local_redirect("/hauptmeister/contest/editrating/$id_contest/$id_cuser", 302);
		exit;
	}
	
	rtl_reply_html(tpl_load('layout/layout_hauptmeister.php', array(
		'title' => 'CTF :: Contest :: Выбираем команду, которой надо подровнять рейтинг',
		'content' => tpl_load('pages/' . implode('/', CKernel::$reqpath) . '-selectuser.php', array(
			'messages' => CKernel::KeGetMessages()
		))
	)));
}







