<?

class CLibUser {

	public static $profile = NULL;
	public static $cteam = NULL;

	/**
	 * Register new command, 0=school, 1=university
	 * @return int user index, or 0 if fail
	 */

	public static function CreateUser($data, $typeindex){
		// min and max numbers of participants
		static $fio_minmax = [1, 7];

		static $ff_base = [
			'fio_captain' => [
				3,
				256,
				'Укажите ФИО капитана',
				'Превышена максимальная длина поля (256 байт)'
			],
			'fio_alles' => [
				3,
				4096,
				'Укажите ФИО участников',
				'Превышена максимальная длина поля (4096 байт)'
			],
			'name_team' => [
				3,
				256,
				'Укажите имя вашей команды',
				'Превышена максимальная длина поля (256 байт)'
			],
			'logo_path' => [],
			'school' => [
				1,
				256,
				['Необходимо указать школу', 'Необходимо указать университет'],
			],
			'klasse' => [
				1,
				256,
				['Необходимо указать класс', 'Необходимо указать курс'],
				'Превышена максимальная длина поля (4096 байт)'
			],
			'contacts' => [
				1,
				4096,
				'Необходимо указать контактные данные',
				'Превышена максимальная длина поля (4096 байт)'
			]
		];

		$fehler = [];
		$wata = [];

		foreach($ff_base as $wort => $reihe){
			$wata[$wort] = trim((string) $data[$wort]);
		}

		$is_okay = TRUE;

		// check for field length
		foreach($ff_base as $wort => $reihe){
			if(count($reihe) > 0){
				$slen = strlen($wata[$wort]);
				if($slen < $reihe[0]){
					CKernel::KeRegisterMessage(is_array($reihe[2]) ? $reihe[2][$typeindex] : $reihe[2], CKernel::ERROR_LEVEL_WARNING);
					$is_okay = FALSE;
					$fehler[] = $wort;
				}else if($slen > $reihe[1]){
					CKernel::KeRegisterMessage(is_array($reihe[3]) ? $reihe[3][$typeindex] : $reihe[3], CKernel::ERROR_LEVEL_WARNING);
					$is_okay = FALSE;
					$fehler[] = $wort;
				}
			}
		}

		// check for unique team name
		if($is_okay){
			if(rtl_database_select([
				'COUNT(cc.id)'
			], 'ctf_teams cc', FALSE, [
				"cc.name_team = '" . addslashes($wata['name_team']) . "'"
			], FALSE, FALSE, 0, 0, $found_rows = NULL, O_SELECT_ONE_VALUE)){
				CKernel::KeRegisterMessage('Имя команды занято! Придумайте другое имя.', CKernel::ERROR_LEVEL_WARNING);
				$is_okay = FALSE;
				$fehler[] = 'name_team';
			}
		}


		// check participants list
		if($is_okay){
			$parcip = trim(preg_replace('#[\r\n]+#Sui', "\n", $wata['fio_alles']));

			$mitglied = 1 + substr_count($parcip, "\n");

			if($mitglied < $fio_minmax[0]){
				CKernel::KeRegisterMessage('Команда должна иметь хотя-бы одного участника!', CKernel::ERROR_LEVEL_WARNING);
				$is_okay = FALSE;
				$fehler[] = 'fio_alles';
			}else if($mitglied > $fio_minmax[1]){
				CKernel::KeRegisterMessage('Команда не может иметь более 7 участников!', CKernel::ERROR_LEVEL_WARNING);
				$is_okay = FALSE;
				$fehler[] = 'fio_alles';
			}

			$wata['fio_alles'] = $parcip;
		}


		if(!$is_okay) return $fehler;

		$wata['typeindex'] = (int) $typeindex;
		$wata['date_reg'] = date('Y-m-d H:i:s');

		// try to create a user
		$resuka = rtl_database_insert('ctf_teams', $wata);
		if($resuka){
			$_SESSION['formifileine'] = [];
		}
		return $resuka;
	}

	public static function DetermineLogotype(){


		if(!is_array($_SESSION['formifileine'])) $_SESSION['formifileine'] = [];

		if(count($lopik = rtl_files_upload($_FILES['logo_path'], '/logotypes')) > 0){
			$_SESSION['formifileine'][] = $lopik[0]['filename'];
		}

		$keks = count($_SESSION['formifileine']);

		if($keks > 0){
			return $_SESSION['formifileine'][$keks - 1];
		}else{
			return NULL; // xui vam
		}
	}

	/**
	 * Oh mein Gott! Verdammte Scheiße! Gefickt in Mund!
	 */

	public static function HashPasswort($pa){
		return hash('sha256', "\x06" . arshloch(arshloch(sha1($pa . 'pizda')) . hash('ripemd160', $pa)) . CKernel::$config['das-secret'] . 'Was ist Das?' . "\x07");
	}

	public static function CreateCUser($id_team, $passwort){
		return rtl_database_insert('ctf_cusers', [
			'id_team' => (int) $id_team,
			'ph' => self::HashPasswort($passwort)
		]);
	}

	/**
	 * Loads user profile from session
	 * @return bool logged or not
	 */

	public static function LoadCProfile(){
		if(NULL !== self::$profile){
			return FALSE !== self::$profile;
		}

		if($_SESSION['uid']){

			$idu = (int) $_SESSION['uid'];

			$cuser = rtl_database_select('*', 'ctf_cusers', FALSE, [
				"id = $idu"
			], FALSE, FALSE, FALSE, FALSE, $durak = NULL, O_SELECT_FIRST_ROW);

			if($cuser){
				self::$profile = $cuser;
				self::$cteam = rtl_database_select('*', 'ctf_teams', FALSE, [
					"id = $cuser->id_team"
				], FALSE, FALSE, FALSE, FALSE, $durak, O_SELECT_FIRST_ROW);

				return TRUE;
			}else{
				self::$profile = FALSE;
				self::$cteam = FALSE;

				CKernel::KeRegisterMessage("Authenticated used does #$idu not exists!", CKernel::ERROR_LEVEL_WARNING);
			}
		}
		return FALSE;
	}

	/**
	 * Loads user profile, verify password, and set session uid
	 * @return bool operaion success flag
	 */

	public static function AuthenticateCProfile($lg, $pw){
		if(!preg_match(RTL_REGEXP_VARNAME, $lg)){
			CKernel::KeRegisterMessage("Недопустимый логин", CKernel::ERROR_LEVEL_WARNING);
			return FALSE;
		}

		$cuser = rtl_database_select('*', 'ctf_cusers', FALSE, [
			"lg='$lg'"
		], FALSE, FALSE, FALSE, FALSE, $durak = NULL, O_SELECT_FIRST_ROW);

		if($cuser){

			if($cuser->ph === self::HashPasswort($pw)){
				$_SESSION['uid'] = (int) $cuser->id;

				self::$profile = $cuser;
				self::$cteam = rtl_database_select('*', 'ctf_teams', FALSE, [
					"id = $cuser->id_team"
				], FALSE, FALSE, FALSE, FALSE, $durak, O_SELECT_FIRST_ROW);

				return TRUE;
			}else{
				CKernel::KeRegisterMessage("Неверный пароль", CKernel::ERROR_LEVEL_WARNING);
			}

		}else{
			CKernel::KeRegisterMessage("Такого пользователя не существует", CKernel::ERROR_LEVEL_WARNING);
		}

		return FALSE;
	}
}
