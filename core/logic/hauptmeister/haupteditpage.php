<?php

lib_load('meister.php');
lib_load('users.php');

$id_page = (int) array_shift(CKernel::$request);

if($_POST['send_page']){
	
	$die_fielde = [
		'title',
		'h1ead',
		'etext',
	];
	
	$wata = [];
	
	foreach($die_fielde as $zeile){
		$wata[$zeile] = $_POST[$zeile];
	}
	
	$wata['is_disabled'] = !!$_POST['is_disabled'];
		
	if($id_page){

		if(rtl_database_update('ctf_epages', $wata, [
			"id=$id_page"
		])){
			CKernel::KeRegisterMessage('Страница обновлена', CKernel::ERROR_LEVEL_INFO);
		}else{
			CKernel::KeRegisterMessage('Что-то не пошло', CKernel::ERROR_LEVEL_WARNING);
		}

	}else{
		if($id_page = rtl_database_insert('ctf_epages', $wata)){
			CKernel::KeRegisterMessage('Страница создана', CKernel::ERROR_LEVEL_INFO);
			CKernel::KeSaveMessages();
			rtl_local_redirect("/hauptmeister/haupteditpage/$id_page", 302);
			exit;
		}else{
			CKernel::KeRegisterMessage('Что-то не пошло', CKernel::ERROR_LEVEL_WARNING);
		}
	}
}

$e_page = $id_page ? CLibMeister::LoadEpageAllein($id_page) : NULL;

CKernel::KeRestoreMessages();

rtl_reply_html(tpl_load('layout/layout_hauptmeister.php', array(
	'title' => "CTF :: Meister :: Редактируем страницу №$id_page ($e_page->alias)",
	'content' => tpl_load('pages/' . implode('/', CKernel::$reqpath) . '.php', array(
		'e_page' => $e_page,
		'messages' => CKernel::KeGetMessages()
	))
)));