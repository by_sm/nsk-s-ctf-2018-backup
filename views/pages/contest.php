<h1>Активные контесты</h1>
<table class="z-table">
	<tr>
		<th>Контест</th>
		<th>Статус</th>
		<th>Действия</th>
	</tr>
	<? foreach($das_list as $li){?>
	<tr>
		<td><?=$li->name_contest?></td>
		<td>
			<? if((int) $li->is_gehen){ ?>
				Контест идет, до окончания: <span style="font-weight: bold" class="es-geht" das:id-contest="<?=$li->id?>" das:es-geht="<?=(int)$li->es_geht?>"><?=rtl_utils_date_interval($li->es_geht, ':', NULL, O_DALW_HOUR | O_DALW_MINUTE | O_DALW_SECOND | O_DINT_DAY | O_DINT_HOUR | O_DINT_MINUTE | O_DINT_SECOND)?></span>
			<? }else if(!(int) $li->is_schluss){?>
				До начала контеста: <span style="font-weight: bold" class="es-wird" das:id-contest="<?=$li->id?>" das:es-wird="<?=(int)$li->es_wird?>"><?=rtl_utils_date_interval($li->es_wird, ':', NULL, O_DALW_HOUR | O_DALW_MINUTE | O_DALW_SECOND | O_DINT_DAY | O_DINT_HOUR | O_DINT_MINUTE | O_DINT_SECOND)?></span>
			<? }else{?>
				Контест закончен
			<? }?>
		</td>
		<td>
			<? if($li->is_gehen || $li->is_schluss){ ?>
			<a href="/contest/<?=$li->id?>">Войти</a>
			<? }?>
		</td>
	</tr>
	<? }?>
</table>