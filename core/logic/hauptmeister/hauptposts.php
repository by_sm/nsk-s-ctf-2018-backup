<?php

lib_load('meister.php');



if($_GET['rspost']){
	
	if(CLibMeister::SetPostRemoved($_GET['rspost'], 0)){
		CKernel::KeRegisterMessage('Пост восстановлен', 0);
	}else{
		CKernel::KeRegisterMessage('Что-то пошло не так', 0);
	}

	CKernel::KeSaveMessages();
	
	rtl_local_redirect('/hauptmeister/hauptposts?'.  rtl_get_merge_parameters(['showhidden'], [], TRUE), 302);
	
	exit;
}

if($_GET['rmpost']){
	
	if(CLibMeister::SetPostRemoved($_GET['rmpost'], 1)){
		CKernel::KeRegisterMessage('Пост скрыт от лишних глаз', 0);
	}else{
		CKernel::KeRegisterMessage('Что-то пошло не так', 0);
	}
		
	CKernel::KeSaveMessages();
	
	rtl_local_redirect('/hauptmeister/hauptposts?'.  rtl_get_merge_parameters(['showhidden'], [], TRUE), 302);
	exit;
}


if($_GET['garbagecollect']){
	$rmv = (int) CLibMeister::GarbageCollectPosts();
	
	CKernel::KeRegisterMessage("Старые посты безвозвратно удалены", 0);
	
	CKernel::KeSaveMessages();
	
	rtl_local_redirect('/hauptmeister/hauptposts?'.  rtl_get_merge_parameters(['showhidden'], [], TRUE), 302);
	exit;
}

$epota = CLibMeister::LoadPostsAlles((int) $_GET['showhidden']);

CKernel::KeRestoreMessages();

rtl_reply_html(tpl_load('layout/layout_hauptmeister.php', array(
	'title' => 'CTF :: Meister :: Исследуем посты',
	'content' => tpl_load('pages/' . implode('/', CKernel::$reqpath) . '.php', array(
		'epota' => $epota,
		'messages' => CKernel::KeGetMessages()
	))
)));