<?=tpl_load('print-gemeister.php', array(
	'messages' => $messages
))?>
<form action="" method="post">
	<div>
		<table class="h-forme">
			<tr>
				<td>ID родителя:</td>
				<td><input type="text" name="id_epage" value="<?=htmlspecialchars(trim($der_post->id_epage))?>" /></td>
			</tr>
			<tr>
				<td>Заголовко:</td>
				<td><input type="text" name="heading" value="<?=htmlspecialchars(trim($der_post->heading))?>" /></td>
			</tr>
			<tr>
				<td>Текст:</td>
				<td>
					<textarea class="tiny-mce" name="postbody"><?=htmlspecialchars(trim($der_post->postbody))?></textarea>
				</td>
			</tr>
			<tr>
				<td>Дата поста:</td>
				<td><input type="text" name="date_post" value="<?=htmlspecialchars(trim($der_post->date_post))?>" /></td>
			</tr>
			<tr>
				<td>Пост удалён:</td>
				<td><input type="checkbox" name="is_removed" value="eins" <?=$der_post->is_removed ? 'checked="checked"' : ''?> /></td>
			</tr>
			<tr>
				<td><input type="submit" name="send_post" value="Aufstellen" /></td>
			</tr>
		</table>
	</div>
</form>
<?=tpl_load('print-gemeister.php', array(
	'messages' => $messages
))?>