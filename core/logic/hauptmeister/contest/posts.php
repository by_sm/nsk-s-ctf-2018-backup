<?

if(count(CKernel::$request) < 2){
	echo "Eure Mutter ist eine Schlampe";
	exit;
}

$s_kind = array_shift(CKernel::$request);
$id_object = (int) array_shift(CKernel::$request);

$die_reihe = [];
$der_kopf = '';

switch($s_kind){
	case 'contest': {
		$der_kopf = "Посты контеста #$id_object";
		$die_reihe = CLibContest::LoadContestPostsVorDer(1, $id_object, !!$_GET['showhidden']);
	} break;

	case 'cat': {
		$der_kopf = "Посты категории #$id_object";
		$die_reihe = CLibContest::LoadContestPostsVorDer(2, $id_object, !!$_GET['showhidden']);
	} break;

	case 'task': {
		$der_kopf = "Посты тасочки #$id_object";
		$die_reihe = CLibContest::LoadContestPostsVorDer(3, $id_object, !!$_GET['showhidden']);
	} break;

	default: {
		echo 'Gehen Sie etwas schlafen';
		exit;
	} break;
}

rtl_reply_html(tpl_load('layout/layout_hauptmeister.php', array(
	'title' => 'CTF :: Contest :: ' . $der_kopf,
	'content' => tpl_load('pages/' . implode('/', CKernel::$reqpath) . '.php', array(
		's_kind' => $s_kind,
		'id_object' => $id_object,
		'die_reihe' => $die_reihe,
		'messages' => CKernel::KeGetMessages()
	))
)));