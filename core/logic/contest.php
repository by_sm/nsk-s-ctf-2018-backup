<?

//if($_SERVER['REMOTE_ADDR'] !== '37.194.225.79') die('Access forbidden: TEST MODE');

lib_load('users.php');
lib_load('contest.php');

if(count(CKernel::$request) > 0){
	$id_contest = (int) array_shift(CKernel::$request);
	
	$coo = CLibContest::LoadContestsAlles($id_contest);
	
	
	if(1 === count($coo)){
		CLibContest::$current_contest = array_shift($coo);
	}
	
	if(CLibContest::$current_contest){
		
		if(CLibContest::$current_contest->is_active && (CLibContest::$current_contest->is_gehen || CLibContest::$current_contest->is_schluss)){
			
			session_start();
			CLibUser::LoadCProfile();
			
			CKernel::KeContinue('standings');
			exit;
		}else{
			echo "Контест не активен, или еще не начался";
			exit;
		}
	}else{
		echo "Invalid contest";
		exit;
	}
	
}else{
	$das_list = CLibContest::LoadContestsAlles();
	$die_lust = [];
	
	foreach($das_list as $bist) if($bist->is_active) $die_lust[] = $bist;

	rtl_reply_html(tpl_load('layout/layout_contest.php', array(
		'title' => 'Активные контесты',
		'content' => tpl_load('pages/' . implode('/', CKernel::$reqpath) . '.php', array(
			'das_list' => $die_lust
		))
	)));
}