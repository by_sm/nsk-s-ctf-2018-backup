<h1>Интерфейс участника</h1>

<?=tpl_load('contest-menu.php')?>

<? foreach($die_werk as $kut){?>
<div class="z-beton">
<h4><?=$kut->name_cat?></h4>
	<? if($kut->tuzik){?>
	
<table class="z-table z-kontest">
		<tr>
			<th>Название</th>
			<th>Стоимость</th>
			<th>Действия</th>
		</tr>
		<? foreach($kut->tuzik as $tuz){?>
		<tr>
			<td><?=$tuz->name_use ? $tuz->name_use : $tuz->name_task?></td>
			<td><?=$tuz->points?></td>
			<td>
				<? if($tuz->is_success){?>
				<div class="z-erfolgreich">Решено (+<?=$tuz->n_tries > 1 ? $tuz->n_tries - 1 : ''?>)</div>
				<a href="/contest/<?=$das_contest->id?>/private/try/<?=$tuz->id_tuzik?>">Посмотреть задачу</a>
				<? }elseif($das_contest->is_gehen){?>
					<? if($tuz->is_allowed){?>
						<a href="/contest/<?=$das_contest->id?>/private/try/<?=$tuz->id_tuzik?>">Решать</a>
						<? if($tuz->n_tries > 0){?>
						<span class="z-fehler">(-<?=$tuz->n_tries?>)</span>
						<? }?>
					<?}else{?>
						Недоступна
					<?}?>
				<? }?>
				
			</td>
		</tr>
		<? }?>
	</table>

	<? }?>
</div>
<? }?>
