<?

// alles tasken sind loaded
$die_werk = CLibContest::$current_contest->katzen;

CLibContest::ExtendTaskListForCUser(
	$die_werk,
	(int) CLibContest::$current_contest->id,
	(int) CLibUser::$profile->id
);

rtl_reply_html(tpl_load('layout/layout_contest.php', array(
	'title' => 'Интерфейс участника',
	'content' => tpl_load('pages/' . implode('/', CKernel::$reqpath) . '.php', array(
		'die_werk' => $die_werk,
		'das_contest' => CLibContest::$current_contest
	))
)));