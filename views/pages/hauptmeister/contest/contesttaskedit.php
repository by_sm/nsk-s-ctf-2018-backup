<?=tpl_load('contest-menuet.php')?>
<?=tpl_load('print-gemeister.php', array(
	'messages' => $messages
))?>
<form action="" method="post">
	<div>
		<table class="h-forme">
			<tr>
				<td>Изменить название:</td>
				<td><input type="text" name="name_use" value="<?=htmlspecialchars(trim($der_tuzik->name_use))?>" /></td>
			</tr>
			<tr>
				<td>Очки:</td>
				<td><input type="text" name="points" value="<?=htmlspecialchars((int)trim($der_tuzik->points))?>" /></td>
			</tr>
			<tr>
				<td>Используемый таск:</td>
				<td>
					<?=tpl_load('combobox.php', [
						'pairs' => CLibContest::LoadTasksPairsSet(),
						'name' => 'id_task',
						'selected' => (int)$der_tuzik->id_task
					])?>
				</td>
			</tr>
			<tr>
				<td>Активность:</td>
				<td><input type="checkbox" name="is_task_active" value="eins" <?=$der_tuzik->is_task_active ? 'checked="checked"' : ''?> /></td>
			</tr>
			<tr>
				<td>Удалено:</td>
				<td><input type="checkbox" name="is_task_removed" value="eins" <?=$der_tuzik->is_task_removed ? 'checked="checked"' : ''?> /></td>
			</tr>
			<tr>
				<td><input type="submit" name="aufstellen_tuzik" value="Aufstellen" /></td>
			</tr>
		</table>
	</div>
</form>
<?=tpl_load('print-gemeister.php', array(
	'messages' => $messages
))?>