<?

class CLibContest {

	public static $current_contest = NULL;

	public static function LoadTasksAlles($is_removed = FALSE){
		
		$sigma = [];
		
		if(FALSE === $is_removed){
			$sigma[] = 't.is_removed = 0';
		}
		
		return rtl_database_select([
			't.*'
		], 'ctf_tasks t', FALSE, $sigma, [
			't.id' => 0
		]);
	}

	public static function LoadTasksPairsSet(){
		return rtl_array_reindex(self::LoadTasksAlles(), 'id', 'name_task');
	}

	public static function  LoadCUsersTeasPairsSet(){
		return rtl_array_reindex(self::LoadCUsersAllesDamit(), 'id', 'name_team');
	}

	public static function LoadCUserPointsOffsetRecord($id_contest, $id_cuser){
		$id_contest = (int) $id_contest;
		$id_cuser = (int) $id_cuser;
		
		return rtl_database_select([
			'*'
		], 'ctf_conrating', FALSE, [
			"id_contest = $id_contest",
			"id_user = $id_cuser"
		], FALSE, FALSE, 0, 0, $durak = FALSE, O_SELECT_FIRST_ROW);
	}

	public static function ReplaceCUserPointsOffsetRecord($record){
		if(is_object($record)) $record = (array) $record;
		
		rtl_database_insert('ctf_conrating', $record, O_INSERT_REPLACE);
		return TRUE;
	}

	/**
	 * Loads the contest table, or single contest with data
	 */
	
	public static function LoadContestsAlles($id_contest = FALSE, $is_removed = FALSE){
		
		$shiva = [];
		$krivo = [];
		$pivko = [];
		
		if(FALSE !== $id_contest){
			$id_contest = (int) $id_contest;
			$shiva[] = "id=$id_contest";
		}
		
		if(FALSE === $is_removed){
			$shiva[] = '0 = c.is_removed';
			$krivo[] = '0 = is_removed';
			$pivko[] = '0 = tu.is_task_removed';
			$pivko[] = '0 = ts.is_removed';
		}
		
		$konti = rtl_array_reindex(rtl_database_select([
			'c.*',
			'es_wird' => 'TIMESTAMPDIFF(SECOND, NOW(), c.date_start)',
			'es_geht' => 'c.lange * 60 + TIMESTAMPDIFF(SECOND, NOW(), c.date_start)',
			'is_gehen' => 'NOW() > c.date_start && c.is_active && !c.is_removed && NOW() < TIMESTAMPADD(SECOND, c.lange * 60, c.date_start)',
			'is_schluss' => 'NOW() >= TIMESTAMPADD(SECOND, c.lange * 60, c.date_start) && !c.is_removed'
		], 'ctf_contest c', FALSE, $shiva, [
			'c.id' => 0
		], FALSE));
		
		if(!$konti) return [];
		
		$bukva = [];
		
		foreach($konti as $iu => $ku){
			$bukva[] = (int) $iu;
			$ku->katzen = [];
		}
		
		$krivo[] = "id_contest " . rtl_sqlc_in($bukva);
		
		$katzen = rtl_database_select('*', 'ctf_taskcat', FALSE, $krivo, [
			'order_cat' => 0
		], FALSE);
		
		
		foreach($katzen as $kater){
			$idct = (int) $kater->id_contest;
			if($konti[$idct]){
				if(!$konti[$idct]->katzen) $konti[$idct]->katzen = [];
				$konti[$idct]->katzen[(int)$kater->id] = $kater;
			}
		}
		
		$pivko[] = "id_contest " . rtl_sqlc_in($bukva);
		
		$hunde = rtl_database_select([
			'tu.*',
			'ts.*',
			'id_tuzik' => 'tu.id',
		], 'ctf_taskuses tu', [
			'LEFT JOIN ctf_tasks ts' => [
				'tu.id_task = ts.id'
			]
		], $pivko, [
			'order_task' => 0
		], FALSE);
		
		foreach($hunde as $hure){
			$idct = (int) $hure->id_contest;
			$idca = (int) $hure->id_cat;
			
			if($konti[$idct]){
				if($konti[$idct]->katzen){
					if($konti[$idct]->katzen[$idca]){
						if(!$konti[$idct]->katzen[$idca]->tuzik) $konti[$idct]->katzen[$idca]->tuzik = [];

						$konti[$idct]->katzen[$idca]->tuzik[(int) $hure->id_task] = $hure;
					}
				}
			}
		}
		
		return $konti;
	}

	public static function LoadContestAllein($id_contest){
		$id_contest = (int) $id_contest;
		return rtl_database_select('*', 'ctf_contest', FALSE, [
			"id=$id_contest"
		], FALSE, FALSE, 0, 0, $durak, O_SELECT_FIRST_ROW);
	}
	
	
	public static function LoadTaskAllein($id_task){
		$id_task = (int) $id_task;
		return rtl_database_select('*', 'ctf_tasks', FALSE, [
			"id=$id_task"
		], FALSE, FALSE, 0, 0, $durak, O_SELECT_FIRST_ROW);
	}
	
	
	public static function LoadKaterAllein($id_kater){
		$id_kater = (int) $id_kater;
		return rtl_database_select('*', 'ctf_taskcat', FALSE, [
			"id=$id_kater"
		], FALSE, FALSE, 0, 0, $durak, O_SELECT_FIRST_ROW);
	}
	
	public static function LoadTuzikAllein($id_tuzik){
		$id_tuzik = (int) $id_tuzik;
		return rtl_database_select([
			'u.*',
			't.*',
			'id_tuzik' =>  'u.id',
			'id_task' =>  't.id'
		], 'ctf_taskuses u', [
			'LEFT JOIN ctf_tasks t' => [
				'u.id_task = t.id'
			]
		], [
			"u.id=$id_tuzik"
		], FALSE, FALSE, 0, 0, $durka = NULL, O_SELECT_FIRST_ROW);
	}
	
	/**
	 * Load one user or alle users
	 */
	
	public static function LoadCUsersAllesDamit($id_user = FALSE, $is_removed = FALSE){

		$damit = [];
		
		if($id_user = (int) $id_user){
			$damit[] = "c.id=$id_user";
		}
		
		if(FALSE === $is_removed){
			$damit[] = 'c.is_removed = 0';
			$damit[] = 't.is_removed = 0';
		}
		
		return rtl_database_select([
			'c.*',
			't.name_team',
		], 'ctf_cusers c', [
			'LEFT JOIN ctf_teams t' => [
				'c.id_team = t.id'
			]
		], $damit, [
			
		]);
	}
	
	
	public static function AllocateUniqueOrderForContest($id_contest){
		$id_contest = (int) $id_contest;
		
		$izverg = rtl_database_select([
			'das' => '1 + MAX(order_cat)'
		], 'ctf_taskcat', FALSE, [
			"id_contest=$id_contest"
		], FALSE, FALSE, 0, 0, $durak = NULL, O_SELECT_ONE_VALUE);
		
		return $izverg ? (int) $izverg : 1;
	}
	
	public static function AllocateUniqueOrderForKater($id_kater){
		$id_kater = (int) $id_kater;
		
		$izverg = rtl_database_select([
			'das' => '1 + MAX(order_task)'
		], 'ctf_taskuses', FALSE, [
			"id_cat=$id_kater"
		], FALSE, FALSE, 0, 0, $durak = NULL, O_SELECT_ONE_VALUE);
		
		return $izverg ? (int) $izverg : 1;
	}
	
	public static function BewegtDerKaterBeiden($id_kater, $is_uber = 1){
		$kater = self::LoadKaterAllein($id_kater);
		$id_mutter = (1 << 31) - 1; //self::AllocateUniqueOrderForContest((int) $kater->id_contest);
		
		$whoren = [
			'id_contest = ' . ((int) $kater->id_contest),
			'is_removed = 0'
		];
		$sollen = [];
		
		if($is_uber){
			$whoren[] = "order_cat < $kater->order_cat";
			$sollen = ['order_cat' => 1];
		}else{
			$whoren[] = "order_cat > $kater->order_cat";
			$sollen = ['order_cat' => 0];
		}
		
		
		$vater = rtl_database_select([
			'*'
		], 'ctf_taskcat', FALSE, $whoren, $sollen, FALSE, 0, 1, $durak = FALSE, O_SELECT_FIRST_ROW);
		
		if($vater){
			// swap
			rtl_database_update('ctf_taskcat', ['order_cat' => $id_mutter], ["id = $vater->id"]);
			rtl_database_update('ctf_taskcat', ['order_cat' => (int) $vater->order_cat], ["id = $kater->id"]);
			rtl_database_update('ctf_taskcat', ['order_cat' => (int) $kater->order_cat], ["id = $vater->id"]);
			
			return TRUE;
		}else{
			CKernel::KeRegisterMessage("Не могу подвинуть категорию, она видимо уже максимально подвинута, хотя хуй знает", CKernel::ERROR_LEVEL_WARNING);
			return FALSE;
		}
	}
	
	
	public static function BewegtDerTuzikBeiden($id_tuzik, $is_uber = 1){
		$id_tuzik = (int) $id_tuzik;
		$tuzik = rtl_database_select([
			'*'
		], 'ctf_taskuses', FALSE, [
			"id = $id_tuzik"
		], FALSE, FALSE, 0, 0, $durak = NULL, O_SELECT_FIRST_ROW);
		
		$id_mutter = (1 << 31) - 1; //self::AllocateUniqueOrderForContest((int) $kater->id_contest);

		$whoren = [
			'id_cat = ' . ((int) $tuzik->id_cat),
			'id_contest = ' . ((int) $tuzik->id_contest),
			'is_task_removed = 0'
		];
		
		$sollen = [];
		
		if($is_uber){
			$whoren[] = "order_task < $tuzik->order_task";
			$sollen = ['order_task' => 1];
		}else{
			$whoren[] = "order_task > $tuzik->order_task";
			$sollen = ['order_task' => 0];
		}
		
		
		$vater = rtl_database_select([
			'*'
		], 'ctf_taskuses', FALSE, $whoren, $sollen, FALSE, 0, 1, $durak = FALSE, O_SELECT_FIRST_ROW);
		
		if($vater){
			// swap
			rtl_database_update('ctf_taskuses', ['order_task' => $id_mutter], ["id = $vater->id"]);
			rtl_database_update('ctf_taskuses', ['order_task' => (int) $vater->order_task], ["id = $tuzik->id"]);
			rtl_database_update('ctf_taskuses', ['order_task' => (int) $tuzik->order_task], ["id = $vater->id"]);
			
			return TRUE;
		}else{
			CKernel::KeRegisterMessage("Не могу подвинуть таску, она видимо уже максимально подвинута, хотя хуй знает", CKernel::ERROR_LEVEL_WARNING);
			return FALSE;
		}
	}

	public static function LoadRatingTable($id_contest){
		$id_contest = (int) $id_contest;
		
		$usiki = rtl_array_reindex(rtl_database_select([
			'c.*',
			't.name_team',
			't.logo_path',
			'n_points' => 'IFNULL(SUM(u.points * s.is_success), 0) + IFNULL(r.points, 0)',
			'n_straffe' => 'IFNULL(SUM(TIMESTAMPDIFF(SECOND, o.date_start, s.date_try) * u.points * s.is_success) / 6000.0, 0)',
			'n_tries' => 'IFNULL(COUNT(s.id), 0)'
		], 'ctf_cusers c', [
			'INNER JOIN ctf_teams t' => [
				'c.id_team = t.id'
			],
			'LEFT JOIN ctf_soltries s' => [
				'c.id = s.id_cuser',
				"$id_contest = s.id_contest",
				's.is_removed = 0'
			],
			'LEFT JOIN ctf_taskuses u' => [
				's.id_task = u.id',
				"$id_contest = u.id_contest"
			],
			'CROSS JOIN ctf_contest o' => [],
			'LEFT JOIN ctf_conrating r' => [
				'r.id_user = c.id',
				"r.id_contest = $id_contest"
			]
		], [
			"o.id = $id_contest",
			'(u.points IS NOT NULL || r.points IS NOT NULL)'
		], [
			'n_points' => 1,
			'n_straffe' => 0,
			't.name_team' => 0
		], [
			'c.id'
		]));
		
		$soulstat = rtl_database_select([
			'id_cuser' => 'c.id',
			'id_task' => 'u.id',
			'n_tries' => 'COUNT(s.id)',
			'points' => 'u.points',
			'is_success' => 'BIT_OR(s.is_success)',
			'solve_date' => 'MAX(s.date_try)',
			'solve_moment' => 'TIMESTAMPDIFF(SECOND, o.date_start, MAX(s.date_try))'
		], 'ctf_cusers c', [
			'CROSS JOIN ctf_taskuses u' => [],
			'LEFT JOIN ctf_soltries s' => [
				'c.id = s.id_cuser',
				'u.id = s.id_task'
			],
			'INNER JOIN ctf_contest o' => [
				'u.id_contest = o.id'
			]
		], [
			"u.id_contest = $id_contest",
			"s.id_contest = $id_contest",
			's.is_removed = 0'
		], [
			'c.id' => 0,
			'u.id' => 0
		], [
			'c.id',
			'u.id'
		]);
		
		foreach($soulstat as $so){
			
			
			if(!$usiki[(int) $so->id_cuser]->sorbat) $usiki[(int) $so->id_cuser]->sorbat = [];
			
			$usiki[(int) $so->id_cuser]->sorbat[(int) $so->id_task] = $so;
		}
		
		return $usiki;
	}

	public static function ExtendTaskListForCUser(&$tli, $id_contest, $id_cuser){
		
		$id_contest = (int) $id_contest;
		$id_cuser = (int) $id_cuser;
		
		$dumm = rtl_array_reindex(rtl_database_select([
			's.id_task',
			'n_tries' => 'COUNT(*)',
			'is_success' => 'BIT_OR(s.is_success)',
			'solve_date' => 'MAX(s.date_try)'
		], 'ctf_soltries s', FALSE, [
			"s.id_contest = $id_contest",
			"s.id_cuser = $id_cuser",
			's.is_removed = 0'
		], FALSE, [
			's.id_task'
		]), 'id_task');
		
		foreach($tli as $li){
			if($li->tuzik){
				$zeil = 1;
				
				foreach($li->tuzik as $tu){
					$zur = $dumm[(int) $tu->id_tuzik];

					$tu->n_tries = $zur->n_tries;
					$tu->is_success = $zur->is_success;
					$tu->solve_date = $zur->solve_date;
					$tu->is_allowed = 1; //$zeil;
					$zeil &= $tu->is_success;
				}
			}
		}
	}
	
	
	public static function AcceptSolutionSuggestion($id_contest, $id_cuser, $id_task, $try_flag, $is_success){
		$id_contest = (int) $id_contest;
		$id_cuser = (int) $id_cuser;
		$id_task = (int) $id_task;
		$is_success = (int) $is_success;
		
		$id_soul = rtl_database_insert('ctf_soltries', [
			'id_contest' => $id_contest,
			'id_cuser' => $id_cuser,
			'id_task' => $id_task,
			'try_flag' => $try_flag,
			'is_success' => $is_success
		]);
		
		return $id_soul;
	}
	
	
	public static function LoadTriesLog($w_filtern = []){
		
		return rtl_database_select([
			's.*',
			'u.points',
			'u.name_use',
			'a.name_task',
			't.name_team',
			'a.tflag',
			'k.name_cat',
			'solve_moment' => 'TIMESTAMPDIFF(SECOND, o.date_start, s.date_try)'
		], 'ctf_soltries s', [
			'INNER JOIN ctf_cusers c' => [
				's.id_cuser = c.id'
			],
			'INNER JOIN ctf_teams t' => [
				'c.id_team = t.id'
			],
			'INNER JOIN ctf_taskuses u' => [
				's.id_task = u.id'
			],
			'INNER JOIN ctf_tasks a' => [
				'a.id = u.id_task'
			],
			'INNER JOIN ctf_taskcat k' => [
				'u.id_cat = k.id'
			],
			'INNER JOIN ctf_contest o' => [
				'u.id_contest = o.id'
			],
		], $w_filtern, [
			'date_try' => 1
		]);
	}
	
	
	public static function LoadContestPostsVorDer($id_etype, $id_entity, $is_removed = FALSE){
		$id_etype = (int) $id_etype;
		$id_entity = (int) $id_entity;
		
		$seltern = [
			"id_etype = $id_etype",
			"id_entity = $id_entity"
		];
		
		if(!$is_removed){
			$seltern[] = 'p.is_removed = 0';
		}
		
		return rtl_database_select([
			'p.*'
		], 'ctf_conposts p', FALSE, $seltern, [
			'date_post' => 1
		]);
	}
	
	public static function LoadContestPostAllein($id_post){
		$id_post = (int) $id_post;
		
		return rtl_database_select([
			'*'
		], 'ctf_conposts p', FALSE, [
			"id = $id_post"
		], FALSE, FALSE, 0, 0, $durak = NULL, O_SELECT_FIRST_ROW);
		
	}
	
}