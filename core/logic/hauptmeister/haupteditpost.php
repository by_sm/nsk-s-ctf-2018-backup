<?

lib_load('meister.php');
lib_load('users.php');

$etwas = array_shift(CKernel::$request);
$id_post = 0;
$id_epage = 0;

if('new' === $etwas){
	$id_epage = (int) array_shift(CKernel::$request);
}else{
	$id_post = (int) $etwas;
}

if($_POST['send_post']){
	
	$die_fielde = [
		'id_epage',
		'heading',
		'postbody',
		'date_post',
	];
	
	$wata = [];
	
	foreach($die_fielde as $zeile){
		$wata[$zeile] = $_POST[$zeile];
	}
	
	$wata['is_removed'] = !!$_POST['is_removed'];
	
	if($id_post){

		if(rtl_database_update('ctf_posts', $wata, [
			"id=$id_post"
		])){
			CKernel::KeRegisterMessage('Пост обновлен', CKernel::ERROR_LEVEL_INFO);
		}else{
			CKernel::KeRegisterMessage('Что-то не пошло', CKernel::ERROR_LEVEL_WARNING);
		}

	}else{
		
		$wata['id_epage'] = (int) $wata['id_epage'];
		
		var_dump($wata);
		
		if($id_post = rtl_database_insert('ctf_posts', $wata)){
			CKernel::KeRegisterMessage('Пост создан', CKernel::ERROR_LEVEL_INFO);
			CKernel::KeSaveMessages();
			rtl_local_redirect("/hauptmeister/haupteditpost/$id_post", 302);
			exit;
		}else{
			CKernel::KeRegisterMessage('Что-то не пошло', CKernel::ERROR_LEVEL_WARNING);
		}
	}
}

$der_post = $id_post ? CLibMeister::LoadPostAllein($id_post) : (object) ['id_epage' => $id_epage, 'date_post' => date('Y-m-d H:i:s')];

CKernel::KeRestoreMessages();

rtl_reply_html(tpl_load('layout/layout_hauptmeister.php', array(
	'title' => 'CTF :: Meister :: ' . ($id_post ? "Редактируем пост №$id_post" : "Создаём пост"),
	'content' => tpl_load('pages/' . implode('/', CKernel::$reqpath) . '.php', array(
		'der_post' => $der_post,
		'messages' => CKernel::KeGetMessages()
	))
)));