<?php
	if(!isset($der_zeiger)) $der_zeiger = 0;

	$das_school = ['Школа', 'Университет'];
	$das_kalsse = ['Класс', 'Курс'];

	$der_school = $namenschule ? $namenschule : $das_school[$der_zeiger];
	$der_klasse = $namenklasse ? $namenklasse : $das_kalsse[$der_zeiger];
?>

<form class="pure-form pure-form-aligned" action="" method="post" enctype="multipart/form-data">
	<fieldset>
		<div class="pure-control-group <?=in_array('fio_captain', $fehler)?'error':''?>">
			<label for="captain"><span class="custom-bg">ФИО Капитана</span></label>
			<input pattern=".{8,100}" required title="8 to 100 characters" type="text" name="fio_captain" value="<?=htmlspecialchars(trim($_POST['fio_captain']))?>" id="captain"/>
		</div>
		<div class="pure-control-group <?=in_array('fio_alles', $fehler)?'error':''?>">
			<label for="members"><span class="custom-bg">ФИО Участников</span></label>
			<textarea pattern=".{10}" required title="minimum 10 characters" name="fio_alles" id="members"><?=htmlspecialchars(trim($_POST['fio_alles']))?></textarea>
		</div>
		<div class="pure-control-group <?=in_array('name_team', $fehler)?'error':''?>">
			<label for="team"><span class="custom-bg">Название</span></label>
			<input pattern=".{3,40}" required title="3 to 40 characters" type="text" name="name_team" value="<?=htmlspecialchars(trim($_POST['name_team']))?>" id="team"/>
		</div>
		<div class="pure-control-group <?=in_array('logo_path', $fehler)?'error':''?>">
			<label for="logo"><span class="custom-bg">Логотип</span></label>

			<?php if($_POST['logo_path']){?>
			<span id="teamform-logotype-loaded">
				<span style="display: inline-block">
				<img style="max-width: 200px" src="<?=$_POST['logo_path']?>" /><br />
				<a class="pure-button button-error" href="javascript:;">Убрать этот логотип</a>
				</span>
			</span>
			<input type="hidden" name="logo_kill" value="0" />
			<?php }?>

			<span class="upload-container" id="teamform-logotype-formen" style="<?= $_POST['logo_path'] ? "display: none;" : ''?>">
				<span class="pure-button button-success">Загрузить</span>
				<span class="upload-filename">Файл не выбран</span>
				<input class="upload-button" type="file" name="logo_path" id="logo" />
			</span>
		</div>
		<div class="pure-control-group <?=in_array('school', $fehler)?'error':''?>">
			<label for="school"><span class="custom-bg"><?=$der_school?></span></label>
			<input pattern=".{3, 255}" required title="3 to 255 characters" type="text" name="school" value="<?=htmlspecialchars(trim($_POST['school']))?>" id="school"/>
		</div>
		<div class="pure-control-group <?=in_array('klasse', $fehler)?'error':''?>">
			<label for="class"><span class="custom-bg"><?=$der_klasse?></span></label>
			<input pattern=".{,255}" required title="max 255 characters" type="text" name="klasse" value="<?=htmlspecialchars(trim($_POST['klasse']))?>" id="class"/>
		</div>
		<div class="pure-control-group <?=in_array('contacts', $fehler)?'error':''?>">
			<label for="contacts"><span class="custom-bg">E-mail</span></label>
			<input required pattern="^\S+@(([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,6})$" title="5 to 255 characters" type="email" name="contacts" value="<?=htmlspecialchars(trim($_POST['contacts']))?>" id="contacts"/>
		</div>
		<div class="pure-controls">
			<span class="pure-button button-error" id="register">Зарегистрироваться</span>
			<input type="submit" name="send_registration" value="Зарегистрироваться" />
			<input type="hidden" name="send_registration" value="1" />
			<?php /*<input class="pure-button" type="submit" name="send_registration" value="Зарегистрироваться" /> */ ?>
		</div>
	</fieldset>

</form>
