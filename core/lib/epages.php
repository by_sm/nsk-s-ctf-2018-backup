<?php

class CLibEpages {
	
	public static function LoadPage($page_alias){
		
		return rtl_database_select(array(
			'ep.*'
		), 'ctf_epages ep', FALSE, array(
			"alias = '$page_alias'"
		), FALSE, FALSE, 0, 1, $found_rows, O_SELECT_FIRST_ROW);
	}
	
	
	public static function LoadPosts($page_id){
		$page_id = (int) $page_id;
		return rtl_database_select(array(
			'cp.*'
		), 'ctf_posts cp', FALSE, array(
			"id_epage = $page_id",
			"is_removed=0"
		), [
			'date_post' => 1
		]);
	}
	
	
}