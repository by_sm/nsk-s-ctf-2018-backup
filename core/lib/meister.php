<?

class CLibMeister {
	
	public static function LoadTeams($typeindex, $is_removed){
		$typeindex = (int) $typeindex;
		$is_removed = (int) $is_removed & 1;
		
		$schlagen = ["typeindex=$typeindex"];
		
		if(!$is_removed){
			$schlagen[] = "ct.is_removed=0";
		}
		
		return rtl_database_select([
			'ct.*',
			'cu.lg',
			'id_cuser' => 'cu.id',
			'is_cuser_removed' => 'cu.is_removed'
		], 'ctf_teams ct', [
			'LEFT JOIN ctf_cusers cu' => [
				'ct.id = cu.id_team'
			]
		], $schlagen, array(
			'ct.date_reg' => 1
		));
	}
	
	public static function SetTeamRemoved($id, $reblo){
		$id = (int) $id;
		$reblo = (int) $reblo & 1;
		
		return rtl_database_update('ctf_teams', [
			'is_removed' => $reblo
		], [
			"id=$id"
		]);
	}
	
	public static function SetPostRemoved($id, $reblo){
		$id = (int) $id;
		$reblo = (int) $reblo & 1;
		
		return rtl_database_update('ctf_posts', [
			'is_removed' => $reblo
		], [
			"id=$id"
		]);
	}
	
	public static function FindTeamById($team_id){
		$team_id = (int) $team_id;
		
		$schlagen = [
			"ct.id=$team_id"
		];
		
		return rtl_database_select([
			'ct.*'
		], 'ctf_teams ct', FALSE, $schlagen, FALSE, FALSE, 0, 1, $found_rows, O_SELECT_FIRST_ROW);
	}

	public static function LoadPostAllein($post_id){
		$post_id = (int) $post_id;
		
		$schlagen = [
			"co.id=$post_id"
		];
		
		return rtl_database_select([
			'co.*'
		], 'ctf_posts co', FALSE, $schlagen, FALSE, FALSE, 0, 1, $found_rows, O_SELECT_FIRST_ROW);
	}
	
	public static function LoadEpageAllein($epage_id){
		$epage_id = (int) $epage_id;
		
		$schlagen = [
			"ce.id=$epage_id"
		];
		
		return rtl_database_select([
			'ce.*'
		], 'ctf_epages ce', FALSE, $schlagen, FALSE, FALSE, 0, 1, $found_rows, O_SELECT_FIRST_ROW);
	}

	/**
	 * Загружает вообще всё и вся
	 **/
	
	public static function LoadPostsAlles($is_removed = 0){
		// load pages
		$epages = rtl_array_reindex(rtl_database_select([
			'ce.*'
		], 'ctf_epages ce', FALSE, [], [
			'ce.alias' => 0
		]));
		
		$schlagen = [];
		
		if(!$is_removed){
			$schlagen[] = 'is_removed=0';
		}
		
		// load posts
		$eposts = rtl_array_reindex(rtl_database_select([
			'co.*'
		], 'ctf_posts co', FALSE, $schlagen, [
			'co.date_post' => 1
		]));
		
		foreach($eposts as $eop){
			$id_epage = (int) $eop->id_epage;
			if(!is_array($epages[$id_epage]->eposts)) $epages[$id_epage]->eposts = [];
			$epages[$id_epage]->eposts[(int) $eop->id] = $eop;
		}
		
		return $epages;
	}
	
	public static function GarbageCollectTeams($die_alte = 604800){
		$die_alte = (int) $die_alte;
		
		rtl_database_query("DELETE FROM ctf_teams WHERE is_removed = 1 && TIMESTAMPDIFF(SECOND, date_vermod, NOW()) > $die_alte;");
		
		return mysql_affected_rows(CKernel::$db_link);
	}
	
	
	public static function GarbageCollectPosts($die_alte = 604800){
		$die_alte = (int) $die_alte;
		
		rtl_database_query("DELETE FROM ctf_posts WHERE is_removed = 1 && TIMESTAMPDIFF(SECOND, date_vermod, NOW()) > $die_alte;");
		
		return mysql_affected_rows(CKernel::$db_link);
	}
	
}