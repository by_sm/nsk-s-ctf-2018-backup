<?

if(count(CKernel::$request) < 2){
	echo "Eure Mutter ist eine Schlampe";
	exit;
}


if($id_deactivate = (int) $_GET['deactivate_try']){
	$n_affected = rtl_database_update('ctf_soltries', [
		'is_removed' => 1
	], [
		"id = $id_deactivate"
	]);
	
	if($n_affected){
		CKernel::KeRegisterMessage('Попытка деактивирована', CKernel::ERROR_LEVEL_INFO);
	}else{
		CKernel::KeRegisterMessage('Лыжи не завелись', CKernel::ERROR_LEVEL_WARNING);
	}
	
	CKernel::KeSaveMessages();
	rtl_local_redirect('/' . implode('/', CKernel::$reqpath) . '/' . implode('/', CKernel::$request) . '?' . rtl_get_merge_parameters(['showhidden'], [], TRUE), 302);
	exit;
}

if($id_activate = (int) $_GET['activate_try']){
	$n_affected = rtl_database_update('ctf_soltries', [
		'is_removed' => 0
	], [
		"id = $id_activate"
	]);
	
	if($n_affected){
		CKernel::KeRegisterMessage('Попытка активирована', CKernel::ERROR_LEVEL_INFO);
	}else{
		CKernel::KeRegisterMessage('Лыжи не завелись', CKernel::ERROR_LEVEL_WARNING);
	}
	
	CKernel::KeSaveMessages();
	rtl_local_redirect('/' . implode('/', CKernel::$reqpath) . '/' . implode('/', CKernel::$request) . '?' . rtl_get_merge_parameters(['showhidden'], [], TRUE), 302);
	exit;
}

CKernel::KeRestoreMessages();

$is_removed = !!$_GET['showhidden'];

$keks = [];

if(!$is_removed){
	$keks[] = 's.is_removed = 0';
}


$s_kind = array_shift(CKernel::$request);
$id_object = (int) array_shift(CKernel::$request);

$die_reihe = [];
$der_kopf = '';

switch($s_kind){
	case 'contest': {
		$der_kopf = "Попыки на контесте #$id_object";
		$keks[] = "s.id_contest = $id_object";
		$die_reihe = CLibContest::LoadTriesLog($keks);
	} break;

	case 'cat': {
		$der_kopf = "Попыки в категории #$id_object";
		$keks[] = "u.id_cat = $id_object";
		$die_reihe = CLibContest::LoadTriesLog($keks);
	} break;

	case 'task': {
		$der_kopf = "Попыки таски #$id_object";
		$keks[] = "s.id_task = $id_object";
		$die_reihe = CLibContest::LoadTriesLog($keks);
	} break;

	case 'cuser': {
		
		if(count(CKernel::$request) > 0){
		
			$id_contest = (int) array_shift(CKernel::$request);

			$der_kopf = "Попыки пользователя #$id_object на контесте #$id_contest";
			
			$keks[] = "s.id_cuser = $id_object";
			$keks[] = "s.id_contest = $id_contest";
			
			$die_reihe = CLibContest::LoadTriesLog($keks);
			
		}else{
			echo 'Gehen Sie etwas schlafen';
			exit;
		}
	} break;

	default: {
		echo 'Gehen Sie etwas schlafen';
		exit;
	} break;
}

rtl_reply_html(tpl_load('layout/layout_hauptmeister.php', array(
	'title' => 'CTF :: Contest :: ' . $der_kopf,
	'content' => tpl_load('pages/' . implode('/', CKernel::$reqpath) . '.php', array(
		'die_reihe' => $die_reihe,
		'messages' => CKernel::KeGetMessages()
	))
)));