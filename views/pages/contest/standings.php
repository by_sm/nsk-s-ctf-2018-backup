<h1><?=CLibContest::$current_contest->name_contest?></h1>

<?=tpl_load('contest-menu.php')?>

<? if(count($die_postern)){?>
<h4>Новости контеста</h4>
<?=tpl_load('listopost.php', [
	'die_postern' => $die_postern
])?>
<?}?>

<h4>Положение команд</h4>

<a target="_blank" href="/contest/<?=CLibContest::$current_contest->id?>/ratingtable?refresh=60" style="font-size: 25px">☣ <span style="font-family: 'Comic sans ms'">Открыть таблицу в новом окне</span> ☣</a>
<div><small>* Таблица показывается в ACM стиле. Минусы обозначают только количество неудачных попыток, и на ваш рейтинг <b>никак не влияют</b>.</small></div>

<h4 style="margin-top: 20px">Диаграмма</h4>

<a target="_blank" href="/contest/<?=CLibContest::$current_contest->id?>/ratingdiagram?refresh=60" style="font-size: 25px">☢ <span style="font-family: 'Comic sans ms'">Открыть диаграмму в новом окне</span> ☢</a>