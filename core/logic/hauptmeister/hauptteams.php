<?

lib_load('meister.php');

if($_GET['rsteam']){

	if(CLibMeister::SetTeamRemoved($_GET['rsteam'], 0)){
		CKernel::KeRegisterMessage('Команда восстановлена', 0);
	}else{
		CKernel::KeRegisterMessage('Что-то пошло не так', 0);
	}


	CKernel::KeSaveMessages();

	rtl_local_redirect('/hauptmeister/hauptteams?'.  rtl_get_merge_parameters(['showhidden'], [], TRUE), 302);

	exit;
}


if($_GET['rmteam']){

	if(CLibMeister::SetTeamRemoved($_GET['rmteam'], 1)){
		CKernel::KeRegisterMessage('Команда скрыта от ваших глаз', 0);
	}else{
		CKernel::KeRegisterMessage('Что-то пошло не так', 0);
	}


	CKernel::KeSaveMessages();

	rtl_local_redirect('/hauptmeister/hauptteams?'.  rtl_get_merge_parameters(['showhidden'], [], TRUE), 302);
	exit;
}


if($_GET['garbagecollect']){
	CLibMeister::GarbageCollectTeams();

	CKernel::KeRegisterMessage('Старые команды безвозвратно удалены', 0);

	CKernel::KeSaveMessages();

	rtl_local_redirect('/hauptmeister/hauptteams?'.  rtl_get_merge_parameters(['showhidden'], [], TRUE), 302);
	exit;
}


$teams_school = CLibMeister::LoadTeams(4, $_GET['showhidden']);
$teams_sibsut = CLibMeister::LoadTeams(3, $_GET['showhidden']);

CKernel::KeRestoreMessages();

rtl_reply_html(tpl_load('layout/layout_hauptmeister.php', array(
	'title' => 'CTF :: Meister :: Смотрим на команды',
	'content' => tpl_load('pages/' . implode('/', CKernel::$reqpath) . '.php', array(
		'teams_school' => $teams_school,
		'teams_sibsut' => $teams_sibsut,
		'messages' => CKernel::KeGetMessages()
	))
)));
