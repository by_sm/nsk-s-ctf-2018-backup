<?

if($id_kater = $_GET['moveup_cat']){
	if(CLibContest::BewegtDerKaterBeiden($id_kater, 1)){
		CKernel::KeRegisterMessage('Категорию сдвинули');
	}
	
	CKernel::KeSaveMessages();
	
	rtl_local_redirect('/' . implode('/', CKernel::$reqpath) . '/' . implode('/', CKernel::$request) . '?' . rtl_get_merge_parameters(['showhidden'], [], TRUE), 302);
	exit;
}


if($id_kater = $_GET['movedw_cat']){
	if(CLibContest::BewegtDerKaterBeiden($id_kater, 0)){
		CKernel::KeRegisterMessage('Категорию сдвинули');
	}
	
	CKernel::KeSaveMessages();
	
	rtl_local_redirect('/' . implode('/', CKernel::$reqpath) . '/' . implode('/', CKernel::$request) . '?' . rtl_get_merge_parameters(['showhidden'], [], TRUE), 302);
	exit;
}

if($id_tuzik = $_GET['moveup_tuz']){
	if(CLibContest::BewegtDerTuzikBeiden($id_tuzik, 1)){
		CKernel::KeRegisterMessage('Тасочку сдвинули');
	}
	
	CKernel::KeSaveMessages();
	
	rtl_local_redirect('/' . implode('/', CKernel::$reqpath) . '/' . implode('/', CKernel::$request) . '?' . rtl_get_merge_parameters(['showhidden'], [], TRUE), 302);
	exit;
}

if($id_tuzik = $_GET['movedw_tuz']){
	if(CLibContest::BewegtDerTuzikBeiden($id_tuzik, 0)){
		CKernel::KeRegisterMessage('Тасочку сдвинули');
	}
	
	CKernel::KeSaveMessages();
	
	rtl_local_redirect('/' . implode('/', CKernel::$reqpath) . '/' . implode('/', CKernel::$request) . '?' . rtl_get_merge_parameters(['showhidden'], [], TRUE), 302);
	exit;
}


$cll = CLibContest::LoadContestsAlles(FALSE, (bool) $_GET['showhidden']);
	
CKernel::KeRestoreMessages();

rtl_reply_html(tpl_load('layout/layout_hauptmeister.php', array(
	'title' => 'CTF :: Contest :: Список контестов',
	'content' => tpl_load('pages/' . implode('/', CKernel::$reqpath) . '.php', array(
		'die_reihe' => $cll,
		'messages' => CKernel::KeGetMessages()
	))
)));