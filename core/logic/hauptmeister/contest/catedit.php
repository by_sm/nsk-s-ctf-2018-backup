<?

lib_load('meister.php');
lib_load('users.php');
lib_load('contest.php');


$id_contest = 0;
$id_kater = 0;
$u_action = NULL;

if(count(CKernel::$request) >= 2){
	//$id_contest = (int) array_shift(CKernel::$request);
	/*switch(array_shift(CKernel::$request)){
		case 'erstellen':
	}*/
	
	$u_action = array_shift(CKernel::$request);
	
}else{
	echo "Eure Mutter ist eine Schlampe";
	exit;
}


switch($u_action){
	case 'vorzeigen': {
		$id_kater = (int) array_shift(CKernel::$request);
	} break;
		
	case 'erstellen': {
		$id_contest = (int) array_shift(CKernel::$request);
	} break;

	default: {
		echo "Eure Mutter ist eine Hure";
		exit;
	} break;	
}


$wata = [
	/*'date_start' => date('Y-m-d H:i:s', time() + 86400 * 14)*/
];

if($_POST['aufstellen_kater']){
	
	$die_fielde = [
		'name_cat'
	];
	
	foreach($die_fielde as $zeile){
		$wata[$zeile] = $_POST[$zeile];
	}
	
	$wata['is_removed'] = !!$_POST['is_removed'];
	
	if('vorzeigen' === $u_action){
		
		if(rtl_database_update('ctf_taskcat', $wata, [
			"id=$id_kater"
		])){
			CKernel::KeRegisterMessage('Категория обновлена', CKernel::ERROR_LEVEL_INFO);
		}else{
			CKernel::KeRegisterMessage('Что-то не пошло', CKernel::ERROR_LEVEL_WARNING);
		}

	}else if('erstellen' === $u_action){
		
		$wata['id_contest'] = $id_contest;
		$wata['order_cat'] = CLibContest::AllocateUniqueOrderForContest($id_contest);
		
		if($id_kater = rtl_database_insert('ctf_taskcat', $wata)){
			CKernel::KeRegisterMessage('Категория запилена', CKernel::ERROR_LEVEL_INFO);
			CKernel::KeSaveMessages();
			rtl_local_redirect("/hauptmeister/contest/catedit/vorzeigen/$id_kater", 302);
			exit;
		}else{
			CKernel::KeRegisterMessage('Что-то не пошло', CKernel::ERROR_LEVEL_WARNING);
		}
	}else{
		echo "Du gefällst mir nicht";
		exit;
	}
}

$der_kater = $id_kater ? CLibContest::LoadKaterAllein($id_kater) : (object) $wata;

CKernel::KeRestoreMessages();

rtl_reply_html(tpl_load('layout/layout_hauptmeister.php', array(
	'title' => 'CTF :: Contest :: ' . ($id_kater ? "Редактируем категорию №$id_kater" : "Создаём категорию в контесте №$id_contest"),
	'content' => tpl_load('pages/' . implode('/', CKernel::$reqpath) . '.php', array(
		'der_kater' => $der_kater,
		'messages' => CKernel::KeGetMessages()
	))
)));