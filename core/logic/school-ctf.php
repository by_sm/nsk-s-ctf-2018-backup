<?php

session_start();

lib_load('epages.php');
lib_load('users.php');
lib_load('kompiler.php');

$epage = CLibEpages::LoadPage('school_ctf');
$epota = CLibEpages::LoadPosts((int) $epage->id);

$fehleren = [];



// hack
if(!$epage->is_disabled && $_POST['send_registration']){
		// check for logo wipe request
	if($_POST['logo_kill']){
		unset($_SESSION['formifileine']);
	}

	$_POST['logo_path'] = CLibUser::DetermineLogotype();

	$was = CLibUser::CreateUser($_POST, 4);

	if(is_integer($was)){

		$epage_okay = CLibEpages::LoadPage('school_ctf_completed');
		$epota_okay = CLibEpages::LoadPosts((int) $epage_okay->id);

		rtl_reply_html(tpl_load('layout/layout_main.php', array(
			'title' => $epage->title,
			'content' => tpl_load('pages/registration.php', array(
				'epage' => $epage,
				'epota' => $epota,
				'epage_okay' => $epage_okay,
				'epota_okay' => $epota_okay,
				'der_zeiger' => 0,
				'econtent' => CLibKompiler::KompileTemplate($epage_okay->etext, [
					'posts' => $epota_okay
				])
			))
		)));
	}else if(is_array($was)){
		$fehleren = $was;
	}
}else{
	unset($_SESSION['formifileine']);
}


if($epage->is_disabled){

	rtl_reply_html(tpl_load('layout/layout_main.php', array(
		'title' => $epage->title,
		'content' => tpl_load('pages/page-disabled.php', [])
	)));

}else{

	$das_messages = CKernel::KeGetMessages();

	rtl_reply_html(tpl_load('layout/layout_main.php', array(
		'title' => $epage->title,
		'content' => tpl_load('pages/' . implode('/', CKernel::$reqpath) . '.php', array(
			'epage' => $epage,
			'epota' => $epota,
			'messages' => CKernel::KeGetMessages(),
			'fehler' => $fehleren,
			'econtent' => CLibKompiler::KompileTemplate($epage->etext, [
				'posts' => $epota,
				'fehler'=> $fehleren,
				'messages' => $das_messages
			])
		))
	)));
}
