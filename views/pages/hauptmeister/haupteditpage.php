<?=tpl_load('print-gemeister.php', array(
	'messages' => $messages
))?>
<form action="" method="post">
	<div>
		<table class="h-forme">
			<tr>
				<td>Заголовко(title):</td>
				<td><input type="text" name="title" value="<?=htmlspecialchars(trim($e_page->title))?>" /></td>
			</tr>
			<tr>
				<td>Заголовко(h1):</td>
				<td><input type="text" name="h1ead" value="<?=htmlspecialchars(trim($e_page->h1ead))?>" /></td>
			</tr>
			<tr>
				<td>Текст:</td>
				<td>
					<textarea name="etext"><?=htmlspecialchars(trim($e_page->etext))?></textarea>
				</td>
			</tr>
			<tr>
				<td>Страница неактивна:</td>
				<td><input type="checkbox" name="is_disabled" value="eins" <?=$e_page->is_disabled ? 'checked="checked"' : ''?> /></td>
			</tr>
			<tr>
				<td><input type="submit" name="send_page" value="Aufstellen" /></td>
			</tr>
		</table>
	</div>
</form>
<?=tpl_load('print-gemeister.php', array(
	'messages' => $messages
))?>