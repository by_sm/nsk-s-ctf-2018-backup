<?php
/*
function CheckHTTPS(){
	return 'https' === $_SERVER['HTTP_X_FORWARDED_PROTO'];
}

if(!checkHTTPS()){
	// HTTPS only
	
	if(preg_match('![\r\n]!', $_SERVER['REQUEST_URI'])){
		die("ERROR");
	}
	
	header("Location: https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]", TRUE, 301);
	exit;
}
*/
session_start();

$immer = $_SERVER['PHP_AUTH_USER'];
$noch = arshloch($_SERVER['PHP_AUTH_PW']);

$okayzer = $immer === CKernel::KeReadVariable('hauptmeister_login') && $noch === CKernel::KeReadVariable('hauptmeister_passwort');

if(!$okayzer){
	header('WWW-Authenticate: Basic realm="CTF::Hauptmeister"');
	header('HTTP/1.0 401 Unauthorized');
	echo 'Please provide valid login and password.<br />';
	echo 'Пожалуйста, укажите валидный логин и пароль.<br />';
	echo 'Bitte zeigen Sie passenden Login und Passwort.<br />';
	exit;
}

// nur zu authorized
CKernel::KeContinue('hauptindex');
