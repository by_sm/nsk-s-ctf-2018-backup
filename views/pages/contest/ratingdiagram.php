<h1><?=CLibContest::$current_contest->name_contest?></h1>

<div style="text-align: center">
	<div id="canvas-holder" style="margin: 30px 0px; max-width: 800px; display: inline-block" >
		<canvas id="chart-area" width="800" height="800" />
	</div>
</div>
<?
	$colors = [
		['#ff2222', '#ff6a6a'],
		['#ffa200', '#ffc45c'],
		['#d510e1', '#e047ea'],
		['#000cff', '#3b44ff'],
		['#00d319', '#40d151'],
		['#b4b4b4', '#cacaca'],
	];

	$chart_data = [];

	$clicli = 0;
	
	foreach($das_rating as $reihe){
		$chart_data [] = [
			'label' => $reihe->name_team,
			'value' => $reihe->n_points,
			'color' => $colors[$clicli] ? $colors[$clicli][0] : '#b4b4b4',
			'highlight' => $colors[$clicli] ? $colors[$clicli][1] : '#cacaca',
		];
		++$clicli;
	}
?>
<? if(count($chart_data) > 1){?>
<script type="text/javascript">
	var chart_data = <?=json_encode($chart_data);?>;
	var ctx = document.getElementById("chart-area").getContext("2d");
	window.cchart = new Chart(ctx).Doughnut(chart_data, {
		animation: false,
		tooltipEvents: [],
		responsive : true,
		showTooltips: true,
		onAnimationComplete: function(){
			this.showTooltip(this.segments, true);
		}

	});
</script>
<? }?>