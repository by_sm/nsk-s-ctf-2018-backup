<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="description" content="NSK CTF - соревнования по защите информации, проводимые в городе городе Новосибирске командой Life (СибГУТИ). Проходят в формате захвата флага" />
	<meta name="keywords" content="ctf, hacking, pentesting, цтф, стф, хакинг, capture the flag, тестирование на проникновение, битвы хакеров, hackforces, белый хакинг, nsk, новосибирск">
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/>
	<link rel="canonical" href="https://nskctf.ru/" />

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter42666234 = new Ya.Metrika({
                    id:42666234,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/42666234" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,800,300&subset=cyrillic,latin' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/pure-min.css"/>
    <link rel="stylesheet" href="css/reset.min.css"/>
	<?php /*
    <link rel="stylesheet/less" type="text/css" href="css/style.less" />
	 */ ?>
	<link rel="stylesheet" type="text/css" href="/css/style.css" />
    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery/2.1.3/jquery.min.js"></script>
    <!--[if IE]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
	<?php /*
    <script>less = {  env: "development"  };</script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/less/2.2.0/less.min.js"></script>
    <script>less.watch();</script>
	 */ ?>
    <script type="text/javascript" src="/js/main.js"></script>
    <title><?=$title?></title>
  </head>
  <body>
    <main role="wrapper">
      <header>
        <div role="banner">
          <h1>SibSUTIS CTF #4</h1> <br/>
            <h2>28 октября</h2>
  <!--        <h2>Стремление делать добро</h2>
          <h2>заложено в нашем коде</h2> -->
        </div>
        <nav role="navigation" class="pure-menu pure-menu-horizontal">
		<?php
			$kerper = ($zu = count(CKernel::$reqpath)) ? CKernel::$reqpath[$zu - 1] : '';
		?>
          <ul class="pure-menu-list">
            <li class="pure-menu-item <?='index'		== $kerper ? 'pure-menu-selected' : ''?>"><a class="pure-menu-link" href="/">Главная</a></li>
            <li class="pure-menu-item <?='sibsutis-ctf' == $kerper ? 'pure-menu-selected' : ''?>"><a class="pure-menu-link" href="/sibsutis-ctf"><b style="color:#b23131;">Sibsutis CTF</b></a></li>
            <li class="pure-menu-item <?='school-ctf'	== $kerper ? 'pure-menu-selected' : ''?>"><a class="pure-menu-link" href="/school-ctf">NSK SCTF</a></li>
            <li class="pure-menu-item <?='nsk-ctf' == $kerper ? 'pure-menu-selected' : ''?>"><a class="pure-menu-link" href="/nsk-ctf">NSK CTF</a></li>
            <li class="pure-menu-item <?='organizators' == $kerper ? 'pure-menu-selected' : ''?>"><a class="pure-menu-link" href="/organizators">Организаторы</a></li>
            <li class="pure-menu-item <?='archive'		== $kerper ? 'pure-menu-selected' : ''?>"><a class="pure-menu-link" href="/archive">Архив заданий</a></li>
            <li class="pure-menu-item <?='contacts'		== $kerper ? 'pure-menu-selected' : ''?>"><a class="pure-menu-link" href="/contacts">Контакты</a></li>
          </ul>
        </nav>
      </header>
      <section>
        <?=$content?>
      </section>
      <footer>
        <div role="content">
          <p>© 1999-2017 Сибирский государственный университет телекоммуникаций и информатики</p>
          <p>630102, г. Новосибирск, ул. Кирова, 86.</p>
          <p>Команда CTF Life</p>
          <p>Design by <b>KAREN-BEE</b></p>
        </div>
      </footer>
    </main>
  </body>
</html>
