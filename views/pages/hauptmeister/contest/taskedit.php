<?=tpl_load('contest-menuet.php')?>
<?=tpl_load('print-gemeister.php', array(
	'messages' => $messages
))?>
<form action="" method="post">
	<div>
		<table class="h-forme">
			<tr>
				<td>Название:</td>
				<td><input type="text" name="name_task" value="<?=htmlspecialchars(trim($die_task->name_task))?>" /></td>
			</tr>
			<tr>
				<td>Флаг:</td>
				<td><input type="text" name="tflag" value="<?=htmlspecialchars(trim($die_task->tflag))?>" /></td>
			</tr>
			<tr>
				<td>Текст задачи:</td>
				<td>
					<textarea class="tiny-mce" name="descr"><?=htmlspecialchars(trim($die_task->descr))?></textarea>
				</td>
			</tr>
			<tr>
				<td>Активность:</td>
				<td><input type="checkbox" name="is_active" value="eins" <?=$die_task->is_active ? 'checked="checked"' : ''?> /></td>
			</tr>
			<tr>
				<td>Удалено:</td>
				<td><input type="checkbox" name="is_removed" value="eins" <?=$die_task->is_removed ? 'checked="checked"' : ''?> /></td>
			</tr>
			<tr>
				<td><input type="submit" name="aufstellen_task" value="Aufstellen" /></td>
			</tr>
		</table>
	</div>
</form>
<?=tpl_load('print-gemeister.php', array(
	'messages' => $messages
))?>