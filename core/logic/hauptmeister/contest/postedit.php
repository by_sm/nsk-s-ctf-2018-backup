<?

if(count(CKernel::$request) < 2){
	echo 'Eure Mutter ist eine scheißen Hure';
	exit;
}

$der_kopf = '';

$id_post = 0;
$s_action = NULL;
$id_etype = 0;
$id_object = 0;

$u_action = array_shift(CKernel::$request);

switch($u_action){
	case 'vorzeigen': {
		$id_post = (int) array_shift(CKernel::$request);
		$der_kopf  = "Редактируем пост #$id_post";
	} break;
		
	case 'erstellen': {
		$s_action = array_shift(CKernel::$request);
		$id_object = (int) array_shift(CKernel::$request);
		
		switch($s_action){
			
			case 'contest': {
				$id_etype = 1;
				$der_kopf  = "Создаем пост для контеста #$id_object";
			} break;
		
			case 'cat': {
				$id_etype = 2;
				$der_kopf  = "Создаем пост для категории #$id_object";
			} break;
		
			case 'task': {
				$id_etype = 3;
				$der_kopf  = "Создаем пост для тасочки #$id_object";
			} break;
		
			default: {
				echo "Verdammte Scheiße!";
				exit;
			} break;
		}
	} break;

	default: {
		echo "Eure Mutter ist eine Hure";
		exit;
	} break;	
}

$wata = [
	'date_post' => rtl_database_query_value('SELECT NOW();')
];

if($_POST['aufstellen_post']){
	$die_fielde = [
		'heading',
		'postbody',
		'date_post'
	];
	
	foreach($die_fielde as $zeile){
		$wata[$zeile] = $_POST[$zeile];
	}
	
	$wata['is_removed'] = !!$_POST['is_removed'];
	
	if('vorzeigen' === $u_action){
		if(rtl_database_update('ctf_conposts', $wata, [
			"id = $id_post"
		])){
			CKernel::KeRegisterMessage('Пост обновлен', CKernel::ERROR_LEVEL_INFO);
		}else{
			CKernel::KeRegisterMessage('Лыжи сломались', CKernel::ERROR_LEVEL_WARNING);
		}
	}else if('erstellen' === $u_action){
		
		$wata['id_etype'] = $id_etype;
		$wata['id_entity'] = $id_object;
		
		if($id_post = rtl_database_insert('ctf_conposts', $wata)){
			CKernel::KeRegisterMessage('Пост запилен', CKernel::ERROR_LEVEL_INFO);
			CKernel::KeSaveMessages();
			rtl_local_redirect("/hauptmeister/contest/postedit/vorzeigen/$id_post", 302);
			exit;
		}else{
			CKernel::KeRegisterMessage('Лыжи проёбаны', CKernel::ERROR_LEVEL_WARNING);
		}
		
	}else{
		echo "Du, Hurensohn, gehst aus!";
		exit;
	}
}

$das_post = !$_POST['aufstellen_post'] && $id_post ? CLibContest::LoadContestPostAllein($id_post) : (object) $wata;

CKernel::KeRestoreMessages();

rtl_reply_html(tpl_load('layout/layout_hauptmeister.php', array(
	'title' => 'CTF :: Contest :: ' . $der_kopf,
	'content' => tpl_load('pages/' . implode('/', CKernel::$reqpath) . '.php', array(
		'das_post' => $das_post,
		'messages' => CKernel::KeGetMessages()
	))
)));