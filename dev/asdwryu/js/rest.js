function serializeFormJSON(a) {
	var o = {};
	$.each(a, function () {
		if (o[this.name]) {
			if (!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};
function getContestDetail(id, type) {
	$("#contestFormBtn").val("edit");
	$("#contestFormBtn").text("Изменить");
	$("#task").addClass("hidden");
	$("#contest").removeClass("hidden");
	$("#tasks").removeClass("hidden");
	$('#contestTasks').empty();
	$("#c_guid").val(id);
	$.ajax({
		type: "GET",
		url: "http://5.8.180.219:8080/api/contest.detail?guid=" + id,
		contentType: "application/json; charset=utf-8",
		success: function(json){
			var data = jQuery.parseJSON(json);
			$("#c_title").val(data.contest.title);
			$("#c_description").val(data.contest.description);
			$("#c_date_start").val(data.contest.date_start);
			$("#c_date_end").val(data.contest.date_end);
			$("#c_max_participants").val(data.contest.max_participants);
			$("#c_rank").val(data.contest.rank);
			$("#c_type").val(data.contest.type);
			$.each(data.tasks, function(i, value) { $('#contestTasks').append($('<div class="col-sm-3 task" onClick="getTaskDetail('+value.guid+');">').text(value.title))});
		},
		failure: function(errMsg) {
			$.notify("<strong>Error</strong> Что-то пошло не так: "+errMsg,{type:"danger",element:"body",placement:{from:"top",align:"center"},offset:20,spacing:10,z_index:1031,delay:2e3,timer:500,mouse_over:null,animate:{enter:"animated fadeInDown",exit:"animated fadeOutUp"},onShow:null,onShown:null,onClose:null,onClosed:null});
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			var errMsg = jQuery.parseJSON(XMLHttpRequest.responseText).message;
			$.notify("<strong>Error</strong> Что-то пошло не так: "+errMsg,{type:"danger",element:"body",placement:{from:"top",align:"center"},offset:20,spacing:10,z_index:1031,delay:2e3,timer:500,mouse_over:null,animate:{enter:"animated fadeInDown",exit:"animated fadeOutUp"},onShow:null,onShown:null,onClose:null,onClosed:null});
		}
	});
}
function getComments(type, id) {
	var url = type === "article" ? "http://5.8.180.219:8080/api/getArticleComments?guid=" : "http://5.8.180.219:8080/api/getNewsComments?guid=";
	$.ajax({
		type: "GET",
		url: url + id,
		contentType: "application/json; charset=utf-8",
		success: function(json){
			var data = jQuery.parseJSON(json);
			$.each(data.items, function(i, value) { $('#newsComments').append($('<div class="col-sm-12 comment">').html("<b class='author'>" + value.author + "</b>:  " + value.text))});
		},
		failure: function(errMsg) {
			$.notify("<strong>Error</strong> Что-то пошло не так: "+errMsg,{type:"danger",element:"body",placement:{from:"top",align:"center"},offset:20,spacing:10,z_index:1031,delay:2e3,timer:500,mouse_over:null,animate:{enter:"animated fadeInDown",exit:"animated fadeOutUp"},onShow:null,onShown:null,onClose:null,onClosed:null});
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			var errMsg = jQuery.parseJSON(XMLHttpRequest.responseText).message;
			$.notify("<strong>Error</strong> Что-то пошло не так: "+errMsg,{type:"danger",element:"body",placement:{from:"top",align:"center"},offset:20,spacing:10,z_index:1031,delay:2e3,timer:500,mouse_over:null,animate:{enter:"animated fadeInDown",exit:"animated fadeOutUp"},onShow:null,onShown:null,onClose:null,onClosed:null});
		}
	});
}
function getNewsDetail(id, type) {
	$("#newsFormBtn").val("edit");
	$("#newsFormBtn").text("Изменить");
	$("#newss").removeClass("hidden");
	$("#newsComments").empty();
	$("#comments").removeClass("hidden");
	$("#n_guid").val(id);
	$.ajax({
		type: "GET",
		url: "http://5.8.180.219:8080/api/news.detail?guid=" + id,
		contentType: "application/json; charset=utf-8",
		success: function(json){
			var data = jQuery.parseJSON(json);
			$("#n_title").val(data.title);
			$("#n_body").val(data.body);
			$("#n_date").val(data.date);
			$("#n_img").val(data.img);
			$("#n_flags").val(data.flags);
			$("#n_tags").val(data.tags);
			// получаем комментарии
			getComments("news", id);
		},
		failure: function(errMsg) {
			$.notify("<strong>Error</strong> Что-то пошло не так: "+errMsg,{type:"danger",element:"body",placement:{from:"top",align:"center"},offset:20,spacing:10,z_index:1031,delay:2e3,timer:500,mouse_over:null,animate:{enter:"animated fadeInDown",exit:"animated fadeOutUp"},onShow:null,onShown:null,onClose:null,onClosed:null});
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			var errMsg = jQuery.parseJSON(XMLHttpRequest.responseText).message;
			$.notify("<strong>Error</strong> Что-то пошло не так: "+errMsg,{type:"danger",element:"body",placement:{from:"top",align:"center"},offset:20,spacing:10,z_index:1031,delay:2e3,timer:500,mouse_over:null,animate:{enter:"animated fadeInDown",exit:"animated fadeOutUp"},onShow:null,onShown:null,onClose:null,onClosed:null});
		}
	});
}
function getArticleDetail(id, type) {
	$("#articleFormBtn").val("edit");
	$("#articleFormBtn").text("Изменить");
	$("#article").removeClass("hidden");
	$("#articleComments").empty();
	$("#comments").removeClass("hidden");
	$("#a_guid").val(id);
	$.ajax({
		type: "GET",
		url: "http://5.8.180.219:8080/api/getArticleDetail?guid=" + id,
		contentType: "application/json; charset=utf-8",
		success: function(json){
			var data = jQuery.parseJSON(json);
			$("#a_title").val(data.title);
			$("#a_body").val(data.body);
			$("#a_date").val(data.date);
			$("#a_img").val(data.img);
			$("#a_flags").val(data.flags);
			$("#a_tags").val(data.tags);
			// получаем комментарии
			getComments("article", id);
		},
		failure: function(errMsg) {
			$.notify("<strong>Error</strong> Что-то пошло не так: "+errMsg,{type:"danger",element:"body",placement:{from:"top",align:"center"},offset:20,spacing:10,z_index:1031,delay:2e3,timer:500,mouse_over:null,animate:{enter:"animated fadeInDown",exit:"animated fadeOutUp"},onShow:null,onShown:null,onClose:null,onClosed:null});
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			var errMsg = jQuery.parseJSON(XMLHttpRequest.responseText).message;
			$.notify("<strong>Error</strong> Что-то пошло не так: "+errMsg,{type:"danger",element:"body",placement:{from:"top",align:"center"},offset:20,spacing:10,z_index:1031,delay:2e3,timer:500,mouse_over:null,animate:{enter:"animated fadeInDown",exit:"animated fadeOutUp"},onShow:null,onShown:null,onClose:null,onClosed:null});
		}
	});
}
function getTaskDetail(id) {
	$("#contest").addClass("hidden");
	$("#task").removeClass("hidden");
	$("#contestForm")[0].reset();
	$("#taskForm")[0].reset();
	$("#taskFormBtn").val("edit");
	$("#taskFormBtn").text("Изменить");
	$("#t_guid").val(id);
	$.ajax({
		type: "GET",
		url: "http://5.8.180.219:8080/api/getTaskDetail?v=2.0&guid=" + id,
		contentType: "application/json; charset=utf-8",
		success: function(json){
			var data = jQuery.parseJSON(json);
			$("#t_title").val(data.title);
			$("#t_keyphrase").val(data.keyphrase);
			$("#t_points").val(data.points);
			$("#t_description").val(data.description);
			$("#t_hints").val(data.hints);
			$("#contestList option[value='"+data.contest_guid+"']").prop('selected', true);
			$("#t_solve").val(data.solve);
			for (i = 0; i < 15; i++) {
				if(data.category & Math.pow(2, i))
				$("#t_category option[value='"+Math.pow(2, i)+"']").prop('selected', true);
				else
				$("#t_category option[value='"+Math.pow(2, i)+"']").prop('selected', false);
			}
			// $("#t_category").val(data.contest.category); категория
			$("#t_tags").val(data.tags);
		},
		failure: function(errMsg) {
			$.notify("<strong>Error</strong> Что-то пошло не так: "+errMsg,{type:"danger",element:"body",placement:{from:"top",align:"center"},offset:20,spacing:10,z_index:1031,delay:2e3,timer:500,mouse_over:null,animate:{enter:"animated fadeInDown",exit:"animated fadeOutUp"},onShow:null,onShown:null,onClose:null,onClosed:null});
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			var errMsg = jQuery.parseJSON(XMLHttpRequest.responseText).message;
			$.notify("<strong>Error</strong> Что-то пошло не так: "+errMsg,{type:"danger",element:"body",placement:{from:"top",align:"center"},offset:20,spacing:10,z_index:1031,delay:2e3,timer:500,mouse_over:null,animate:{enter:"animated fadeInDown",exit:"animated fadeOutUp"},onShow:null,onShown:null,onClose:null,onClosed:null});
		}
	});
}

$(document).ready(function() {
	// get contests list (for tasks.html)
	$.ajax({
		type: "GET",
		url: "http://5.8.180.219:8080/api/contests.get",
		contentType: "application/json; charset=utf-8",
		success: function(json){
			var data = jQuery.parseJSON(json);
			$.each(data.items, function(i, value) {
				$('#contestList.form-control').append($('<option>').text(value.title).attr('value', value.guid));
			});
		},
		failure: function(errMsg) {
			$.notify("<strong>Error</strong> Что-то пошло не так: "+errMsg,{type:"danger",element:"body",placement:{from:"top",align:"center"},offset:20,spacing:10,z_index:1031,delay:2e3,timer:500,mouse_over:null,animate:{enter:"animated fadeInDown",exit:"animated fadeOutUp"},onShow:null,onShown:null,onClose:null,onClosed:null});
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			var errMsg = jQuery.parseJSON(XMLHttpRequest.responseText).message;
			$.notify("<strong>Error</strong> Что-то пошло не так: "+errMsg,{type:"danger",element:"body",placement:{from:"top",align:"center"},offset:20,spacing:10,z_index:1031,delay:2e3,timer:500,mouse_over:null,animate:{enter:"animated fadeInDown",exit:"animated fadeOutUp"},onShow:null,onShown:null,onClose:null,onClosed:null});
		}
	});
	// get contests list (for contests.html)
	$.ajax({
		type: "GET",
		url: "http://5.8.180.219:8080/api/getContests",
		contentType: "application/json; charset=utf-8",
		success: function(json){
			var data = jQuery.parseJSON(json);
			$.each(data.items, function(i, value) {
				$('#contests').append($('<button class="btn-block btn btn-primary btn-xs btn-responsive" type="button">').text(value.title).attr({onClick : 'getContestDetail(' + value.guid + ');', value: value.guid}));
			});
		},
		failure: function(errMsg) {
			$.notify("<strong>Error</strong> Что-то пошло не так: "+errMsg,{type:"danger",element:"body",placement:{from:"top",align:"center"},offset:20,spacing:10,z_index:1031,delay:2e3,timer:500,mouse_over:null,animate:{enter:"animated fadeInDown",exit:"animated fadeOutUp"},onShow:null,onShown:null,onClose:null,onClosed:null});
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			var errMsg = jQuery.parseJSON(XMLHttpRequest.responseText).message;
			$.notify("<strong>Error</strong> Что-то пошло не так: "+errMsg,{type:"danger",element:"body",placement:{from:"top",align:"center"},offset:20,spacing:10,z_index:1031,delay:2e3,timer:500,mouse_over:null,animate:{enter:"animated fadeInDown",exit:"animated fadeOutUp"},onShow:null,onShown:null,onClose:null,onClosed:null});
		}
	});
	$.ajax({
		type: "GET",
		url: "http://5.8.180.219:8080/api/getNews?limit=50",
		contentType: "application/json; charset=utf-8",
		success: function(json){
			var data = jQuery.parseJSON(json);
			$.each(data.items, function(i, value) {
				$('#news').append($('<button class="btn-block btn btn-primary btn-xs btn-responsive" type="button">').text(value.title).attr({onClick : 'getNewsDetail(' + value.guid + ');', value: value.guid}));
			});
		},
		failure: function(errMsg) {
			$.notify("<strong>Error</strong> Что-то пошло не так: "+errMsg,{type:"danger",element:"body",placement:{from:"top",align:"center"},offset:20,spacing:10,z_index:1031,delay:2e3,timer:500,mouse_over:null,animate:{enter:"animated fadeInDown",exit:"animated fadeOutUp"},onShow:null,onShown:null,onClose:null,onClosed:null});
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			var errMsg = jQuery.parseJSON(XMLHttpRequest.responseText).message;
			$.notify("<strong>Error</strong> Что-то пошло не так: "+errMsg,{type:"danger",element:"body",placement:{from:"top",align:"center"},offset:20,spacing:10,z_index:1031,delay:2e3,timer:500,mouse_over:null,animate:{enter:"animated fadeInDown",exit:"animated fadeOutUp"},onShow:null,onShown:null,onClose:null,onClosed:null});
		}
	});
	$.ajax({
		type: "GET",
		url: "http://5.8.180.219:8080/api/getArticles?limit=50",
		contentType: "application/json; charset=utf-8",
		success: function(json){
			var data = jQuery.parseJSON(json);
			$.each(data.items, function(i, value) {
				$('#articles').append($('<button class="btn-block btn btn-primary btn-xs btn-responsive" type="button">').text(value.title).attr({onClick : 'getArticleDetail(' + value.guid + ');', value: value.guid}));
			});
		},
		failure: function(errMsg) {
			$.notify("<strong>Error</strong> Что-то пошло не так: "+errMsg,{type:"danger",element:"body",placement:{from:"top",align:"center"},offset:20,spacing:10,z_index:1031,delay:2e3,timer:500,mouse_over:null,animate:{enter:"animated fadeInDown",exit:"animated fadeOutUp"},onShow:null,onShown:null,onClose:null,onClosed:null});
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			var errMsg = jQuery.parseJSON(XMLHttpRequest.responseText).message;
			$.notify("<strong>Error</strong> Что-то пошло не так: "+errMsg,{type:"danger",element:"body",placement:{from:"top",align:"center"},offset:20,spacing:10,z_index:1031,delay:2e3,timer:500,mouse_over:null,animate:{enter:"animated fadeInDown",exit:"animated fadeOutUp"},onShow:null,onShown:null,onClose:null,onClosed:null});
		}
	});
	// добавляем контест
	$("#addContest").on("click", function(e) {
		$("#contestFormBtn").val("add");
		$("#contestFormBtn").text("Добавить");
		$("#contestForm")[0].reset();
		$("#taskForm")[0].reset();
		$("#task").addClass("hidden");
		$("#contest").removeClass("hidden");
		$("#tasks").addClass("hidden");
		$('#contestTasks').empty();
	});
	$("#addNews").on("click", function(e) {
		$("#newsFormBtn").val("add");
		$("#newsFormBtn").text("Добавить");
		$("#newsForm")[0].reset();
		$("#comments").addClass("hidden");
		$("#news").removeClass("hidden");
		$('#newsComments').empty();
	});
	// добавляем
	$("#addTask").on("click", function(e) {
		$("#taskFormBtn").val("add");
		$("#taskFormBtn").text("Добавить");
		$("#contestForm")[0].reset();
		$("#taskForm")[0].reset();
		$("#task").removeClass("hidden");
		$("#contest").addClass("hidden");
	});
	$("#addArticle").on("click", function(e) {
		$("#articleFormBtn").val("add");
		$("#articleFormBtn").text("Добавить");
		$("#articleForm")[0].reset();
		$("#comments").addClass("hidden");
		$("#article").removeClass("hidden");
		$('#newsComments').empty();
	});
	$("#taskForm").on("submit", function(e) {
		e.preventDefault();
		var arr = $(this).serializeArray();
		var cat = 0;
		$.each( arr, function( index, value ){
			if(value["name"] == "category")
			cat += parseInt(value["value"]);
		});
		var url = $("#taskFormBtn").val() === "edit" ? "http://5.8.180.219:8080/api/editTask" : "http://5.8.180.219:8080/api/addTask";
		arr.push({"name": "mask", "value": cat});
		arr.push({"name": "token", "value": "cb17879b0261c4bb240c3ee7e7af5eaa8feeed40b47411050278c6da32816155"});
		arr = JSON.stringify(serializeFormJSON(_.reject(arr, function(al){ return al["name"] == "category"; })));
		$.ajax({
			type: "POST",
			url: "http://5.8.180.219:8080/api/addTask",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: arr,
			success: function(data){
				$("#taskFormBtn").val() === "add" && $('#taskForm')[0].reset();
				$.notify("<strong>Success</strong> Вы добавили/изменили таск\t...",{type:"success",element:"body",placement:{from:"top",align:"center"},offset:20,spacing:10,z_index:1031,delay:2e3,timer:500,mouse_over:null,animate:{enter:"animated fadeInDown",exit:"animated fadeOutUp"},onShow:null,onShown:null,onClose:null,onClosed:null});
			},
			failure: function(errMsg) {
				$.notify("<strong>Error</strong> Что-то пошло не так: "+errMsg,{type:"danger",element:"body",placement:{from:"top",align:"center"},offset:20,spacing:10,z_index:1031,delay:2e3,timer:500,mouse_over:null,animate:{enter:"animated fadeInDown",exit:"animated fadeOutUp"},onShow:null,onShown:null,onClose:null,onClosed:null});
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				var errMsg = jQuery.parseJSON(XMLHttpRequest.responseText).message;
				$.notify("<strong>Error</strong> Что-то пошло не так: "+errMsg,{type:"danger",element:"body",placement:{from:"top",align:"center"},offset:20,spacing:10,z_index:1031,delay:2e3,timer:500,mouse_over:null,animate:{enter:"animated fadeInDown",exit:"animated fadeOutUp"},onShow:null,onShown:null,onClose:null,onClosed:null});

			}
		});
		return false;
	});
	$("#contestForm").on("submit", function(e) {
		e.preventDefault();
		var arr = $(this).serializeArray();
		var url = $("#contestFormBtn").val() === "edit" ? "http://5.8.180.219:8080/api/editContest" : "http://5.8.180.219:8080/api/addContest";
		arr.push({"name": "token", "value": "cb17879b0261c4bb240c3ee7e7af5eaa8feeed40b47411050278c6da32816155"});
		arr = JSON.stringify(serializeFormJSON(arr));
		$.ajax({
			type: "POST",
			url: url,
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: arr,
			success: function(data){
				$("#contestFormBtn").val() === "add" && $('#contestForm')[0].reset();
				$.notify("<strong>Success</strong> Вы добавили/изменили контест\t...",{type:"success",element:"body",placement:{from:"top",align:"center"},offset:20,spacing:10,z_index:1031,delay:2e3,timer:500,mouse_over:null,animate:{enter:"animated fadeInDown",exit:"animated fadeOutUp"},onShow:null,onShown:null,onClose:null,onClosed:null});
			},
			failure: function(errMsg) {
				$.notify("<strong>Error</strong> Что-то пошло не так: "+errMsg,{type:"danger",element:"body",placement:{from:"top",align:"center"},offset:20,spacing:10,z_index:1031,delay:2e3,timer:500,mouse_over:null,animate:{enter:"animated fadeInDown",exit:"animated fadeOutUp"},onShow:null,onShown:null,onClose:null,onClosed:null});
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				var errMsg = jQuery.parseJSON(XMLHttpRequest.responseText).message;
				$.notify("<strong>Error</strong> Что-то пошло не так: "+errMsg,{type:"danger",element:"body",placement:{from:"top",align:"center"},offset:20,spacing:10,z_index:1031,delay:2e3,timer:500,mouse_over:null,animate:{enter:"animated fadeInDown",exit:"animated fadeOutUp"},onShow:null,onShown:null,onClose:null,onClosed:null});

			}
		});
		return false;
	});
	$("#newsForm").on("submit", function(e) {
		e.preventDefault();
		var arr = $(this).serializeArray();
		var url = $("#newsFormBtn").val() === "edit" ? "http://5.8.180.219:8080/api/editNews" : "http://5.8.180.219:8080/api/addNews";
		arr.push({"name": "token", "value": "cb17879b0261c4bb240c3ee7e7af5eaa8feeed40b47411050278c6da32816155"});
		arr = JSON.stringify(serializeFormJSON(arr));
		$.ajax({
			type: "POST",
			url: url,
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: arr,
			success: function(data){
				$("#newsFormBtn").val() === "add" && $('#newsForm')[0].reset();
				$.notify("<strong>Success</strong> Вы добавили/изменили новость\t...",{type:"success",element:"body",placement:{from:"top",align:"center"},offset:20,spacing:10,z_index:1031,delay:2e3,timer:500,mouse_over:null,animate:{enter:"animated fadeInDown",exit:"animated fadeOutUp"},onShow:null,onShown:null,onClose:null,onClosed:null});
			},
			failure: function(errMsg) {
				$.notify("<strong>Error</strong> Что-то пошло не так: "+errMsg,{type:"danger",element:"body",placement:{from:"top",align:"center"},offset:20,spacing:10,z_index:1031,delay:2e3,timer:500,mouse_over:null,animate:{enter:"animated fadeInDown",exit:"animated fadeOutUp"},onShow:null,onShown:null,onClose:null,onClosed:null});
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				var errMsg = jQuery.parseJSON(XMLHttpRequest.responseText).message;
				$.notify("<strong>Error</strong> Что-то пошло не так: "+errMsg,{type:"danger",element:"body",placement:{from:"top",align:"center"},offset:20,spacing:10,z_index:1031,delay:2e3,timer:500,mouse_over:null,animate:{enter:"animated fadeInDown",exit:"animated fadeOutUp"},onShow:null,onShown:null,onClose:null,onClosed:null});

			}
		});
		return false;
	});
	$("#articleForm").on("submit", function(e) {
		e.preventDefault();
		var arr = $(this).serializeArray();
		var url = $("#articleFormBtn").val() === "edit" ? "http://5.8.180.219:8080/api/editArticle" : "http://5.8.180.219:8080/api/addArticle";
		arr.push({"name": "token", "value": "cb17879b0261c4bb240c3ee7e7af5eaa8feeed40b47411050278c6da32816155"});
		arr = JSON.stringify(serializeFormJSON(arr));
		$.ajax({
			type: "POST",
			url: url,
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: arr,
			success: function(data){
				$("#articleFormBtn").val() === "add" && $('#articleForm')[0].reset();
				$.notify("<strong>Success</strong> Вы добавили/изменили статью\t...",{type:"success",element:"body",placement:{from:"top",align:"center"},offset:20,spacing:10,z_index:1031,delay:2e3,timer:500,mouse_over:null,animate:{enter:"animated fadeInDown",exit:"animated fadeOutUp"},onShow:null,onShown:null,onClose:null,onClosed:null});
			},
			failure: function(errMsg) {
				$.notify("<strong>Error</strong> Что-то пошло не так: "+errMsg,{type:"danger",element:"body",placement:{from:"top",align:"center"},offset:20,spacing:10,z_index:1031,delay:2e3,timer:500,mouse_over:null,animate:{enter:"animated fadeInDown",exit:"animated fadeOutUp"},onShow:null,onShown:null,onClose:null,onClosed:null});
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				var errMsg = jQuery.parseJSON(XMLHttpRequest.responseText).message;
				$.notify("<strong>Error</strong> Что-то пошло не так: "+errMsg,{type:"danger",element:"body",placement:{from:"top",align:"center"},offset:20,spacing:10,z_index:1031,delay:2e3,timer:500,mouse_over:null,animate:{enter:"animated fadeInDown",exit:"animated fadeOutUp"},onShow:null,onShown:null,onClose:null,onClosed:null});

			}
		});
		return false;
	});
});
