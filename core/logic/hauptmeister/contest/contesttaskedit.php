<?

lib_load('meister.php');
lib_load('users.php');
lib_load('contest.php');


$id_kater = 0;
$id_tuzik = 0;
$u_action = NULL;

if(count(CKernel::$request) >= 2){
	//$id_contest = (int) array_shift(CKernel::$request);
	/*switch(array_shift(CKernel::$request)){
		case 'erstellen':
	}*/
	
	$u_action = array_shift(CKernel::$request);
	
}else{
	echo "Eure Mutter ist eine Schlampe";
	exit;
}


switch($u_action){
	case 'vorzeigen': {
		$id_tuzik = (int) array_shift(CKernel::$request);
	} break;
		
	case 'erstellen': {
		$id_kater = (int) array_shift(CKernel::$request);
	} break;

	default: {
		echo "Eure Mutter ist eine Hure";
		exit;
	} break;	
}

$wata = [
	'is_task_active' => 1
];


if($_POST['aufstellen_tuzik']){
	
	$die_fielde = [
		'name_use'
	];
	
	foreach($die_fielde as $zeile){
		$wata[$zeile] = $_POST[$zeile];
	}
	
	$wata['is_task_active'] = !!$_POST['is_task_active'];
	$wata['is_task_removed'] = !!$_POST['is_task_removed'];
	
	$wata['id_task'] = (int) $_POST['id_task'];
	$wata['points'] = (int) $_POST['points'];
	
	if('vorzeigen' === $u_action){
		$jaja = TRUE;
		
		if($tuzik = CLibContest::LoadTuzikAllein($id_tuzik)){
			if($wata['id_task'] != (int) $tuzik->id_task){
				
				$gaz = rtl_database_select([
					'das' => 'COUNT(*)'
				], 'ctf_taskuses', FALSE, [
					"id_task=$wata[id_task]",
					"id_contest=$tuzik->id_contest"
				], FALSE, FALSE, 0, 0, $durak = NULL, O_SELECT_ONE_VALUE);
				
				if($gaz){
					$jaja = FALSE;
					CKernel::KeRegisterMessage('Нельзя в один контест запихать джва одинаковых таска!', CKernel::ERROR_LEVEL_WARNING);
				}
			}
		}
		
		if($jaja){
			if(rtl_database_update('ctf_taskuses', $wata, [
				"id=$id_tuzik"
			])){
				CKernel::KeRegisterMessage('Она обновлена', CKernel::ERROR_LEVEL_INFO);
			}else{
				CKernel::KeRegisterMessage('Лыжи не едут', CKernel::ERROR_LEVEL_WARNING);
			}
		}

	}else if('erstellen' === $u_action){
		
		$wata['order_task'] = CLibContest::AllocateUniqueOrderForKater($id_kater);
		$wata['id_cat'] = $id_kater;
		$wata['id_contest'] = (int) CLibContest::LoadKaterAllein($id_kater)->id_contest;
		
		$jaja = TRUE;
		
		$gaz = rtl_database_select([
			'das' => 'COUNT(*)'
		], 'ctf_taskuses', FALSE, [
			"id_task=$wata[id_task]",
			"id_contest=$wata[id_contest]"
		], FALSE, FALSE, 0, 0, $durak = NULL, O_SELECT_ONE_VALUE);

		if($gaz){
			$jaja = FALSE;
			CKernel::KeRegisterMessage('Нельзя в один контест запихать джва одинаковых таска!', CKernel::ERROR_LEVEL_WARNING);
		}
		
		if($jaja){
			if($id_tuzik = rtl_database_insert('ctf_taskuses', $wata)){
				CKernel::KeRegisterMessage('Она запилена', CKernel::ERROR_LEVEL_INFO);
				CKernel::KeSaveMessages();
				rtl_local_redirect("/hauptmeister/contest/contesttaskedit/vorzeigen/$id_tuzik", 302);
				exit;
			}else{
				CKernel::KeRegisterMessage('Лыжи не едут', CKernel::ERROR_LEVEL_WARNING);
			}
		}
	}else{
		echo "Du gefällst mir nicht";
		exit;
	}
	
}

$der_tuzik = !$_POST['aufstellen_tuzik'] && $id_tuzik ? CLibContest::LoadTuzikAllein($id_tuzik) : (object) $wata;

if($id_tuzik && $der_tuzik){
	$tazik = CLibContest::LoadTaskAllein((int) $der_tuzik->id_task);
	if(!$tazik){
		CKernel::KeRegisterMessage("Обратите внимание, используемая здесь таска #$der_tuzik->id_task была удалена", CKernel::ERROR_LEVEL_INFO);
	}else if($tazik->is_removed){
		CKernel::KeRegisterMessage("Обратите внимание, используемая здесь таска <a href=\"/hauptmeister/contest/taskedit/$der_tuzik->id_task\">$der_tuzik->name_task</a> помечена удаленной, поэтому в списке ее нет!", CKernel::ERROR_LEVEL_INFO);
	}
}


CKernel::KeRestoreMessages();

rtl_reply_html(tpl_load('layout/layout_hauptmeister.php', array(
	'title' => 'CTF :: Contest :: ' . ($id_tuzik ? "Редактируем контесттаску №$id_tuzik" : "Создаём категорию в категории №$id_kater"),
	'content' => tpl_load('pages/' . implode('/', CKernel::$reqpath) . '.php', array(
		'der_tuzik' => $der_tuzik,
		'messages' => CKernel::KeGetMessages()
	))
)));