<?

$id_tuzik = 0;

if(count(CKernel::$request) > 0){
	$id_tuzik = (int) array_shift(CKernel::$request);
}


if(!$id_tuzik){
	echo "Не указана задача";
	exit;
}


// Scheißencode beginnt

$katzen = CLibContest::$current_contest->katzen;

CLibContest::ExtendTaskListForCUser(
	$katzen,
	(int) CLibContest::$current_contest->id,
	(int) CLibUser::$profile->id
);

$die_task = NULL;

foreach(CLibContest::$current_contest->katzen as $kater){
	if($kater->tuzik){
		foreach($kater->tuzik as $lust){
			if((int)$lust->id_tuzik === $id_tuzik){
				$die_task = $lust;
			}
		}
	}
}

if(!$die_task){
	echo "Не найдена задача";
	exit;
}


if(!$die_task->is_allowed && !CLibContest::$current_contest->is_schluss){
	echo "Задача вам пока недоступна";
	exit;
}


if($_POST['send_flag'] && CLibContest::$current_contest->is_gehen){
	
	if(!$_POST['flag'] || 0 == strlen($_POST['flag'])){
		CKernel::KeRegisterMessage("Введите флаг", CKernel::ERROR_LEVEL_WARNING);
	}else{
		
		if($die_task->is_success){
			CKernel::KeRegisterMessage("Вы уже сдали эту задачу", CKernel::ERROR_LEVEL_WARNING);
		}else{
			if($_POST['flag'] === $die_task->tflag){

				CLibContest::AcceptSolutionSuggestion(
					(int) CLibContest::$current_contest->id,
					(int) CLibUser::$profile->id,
					$id_tuzik,
					$_POST['flag'],
					1
				);

				CKernel::KeRegisterMessage("Поздравляем! Задача решена!", CKernel::ERROR_LEVEL_INFO);
				// update solution info
				$die_task->n_tries++;
				$die_task->is_success = TRUE;
				$die_task->solve_date = rtl_database_query_value('SELECT NOW();');

			}else{
				CLibContest::AcceptSolutionSuggestion(
					(int) CLibContest::$current_contest->id,
					(int) CLibUser::$profile->id,
					$id_tuzik,
					$_POST['flag'],
					0
				);

				CKernel::KeRegisterMessage("Неудача! Попробуйте ещё раз", CKernel::ERROR_LEVEL_WARNING);
			}
		}
	}
}


rtl_reply_html(tpl_load('layout/layout_contest.php', array(
	'title' => $die_task->name_task,
	'content' => tpl_load('pages/' . implode('/', CKernel::$reqpath) . '.php', array(
		'die_task' => $die_task,
		'das_contest' => CLibContest::$current_contest,
		'die_postern' => CLibContest::LoadContestPostsVorDer(3, $id_tuzik)
	))
)));