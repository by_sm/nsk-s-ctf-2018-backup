<?

$config = [

	'mdb-host' => 'localhost',
	'mdb-user' => 'root',
	'mdb-password' => 'pinlox123',
	'mdb-database' => 'nskctf',

	'core-default-http-headers' => array(
		'Pragma' => 'no-cache',
		'Cache-Control' => 'no-store, no-cache, must-revalidate, proxy-revalidate, max-age=0',
		'Expires' => 'Mon, 23 Jan 2006 17:37:34 GMT',
		'Last-Modified' => gmdate('D, d M Y H:i:s ').'GMT'
	),

	'das-secret' => 'dhQkvD4dRc0ksozDTXBUUWzB4uCKAcgn',
];
