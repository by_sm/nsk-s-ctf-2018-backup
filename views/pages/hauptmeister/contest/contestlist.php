<?=tpl_load('contest-menuet.php')?>
<?=tpl_load('print-gemeister.php', array(
	'messages' => $messages
))?>

<div>
<? if($_GET['showhidden']){?>
<a href="?<?=rtl_get_merge_parameters([], ['showhidden'=>0], TRUE)?>">Скрыть удалённые</a>
<? }else{?>
<a href="?<?=rtl_get_merge_parameters([], ['showhidden'=>1], TRUE)?>">Показать удалённые</a>
<?}?> |
<a href="/hauptmeister/contest/contestedit">А не запилить ли контест</a>
</div>
<div>Новые контесты &mdash; внизу</div>

<? foreach($die_reihe as $reihe){ ?>
<div class="h-epage <?=$reihe->is_removed ? 'hc-inactive' : ''?>">
	<h2>Contest: <?=$reihe->name_contest?></h2>
	<div>
		Дата старта: <b><?=$reihe->date_start?></b> |
		<? if((int) $reihe->is_gehen){ ?>
			До окончания: <span style="font-weight: bold" class="es-geht" das:id-contest="<?=$reihe->id?>" das:es-geht="<?=(int)$reihe->es_geht?>"><?=rtl_utils_date_interval($reihe->es_geht, ':', NULL, O_DALW_HOUR | O_DALW_MINUTE | O_DALW_SECOND | O_DINT_DAY | O_DINT_HOUR | O_DINT_MINUTE | O_DINT_SECOND)?></span>
		<? }else if(!(int) $reihe->is_schluss){?>
			До начала: <span style="font-weight: bold" class="es-wird" das:id-contest="<?=$reihe->id?>" das:es-wird="<?=(int)$reihe->es_wird?>"><?=rtl_utils_date_interval($reihe->es_wird, ':', NULL, O_DALW_HOUR | O_DALW_MINUTE | O_DALW_SECOND | O_DINT_DAY | O_DINT_HOUR | O_DINT_MINUTE | O_DINT_SECOND)?></span>
		<? }else{?>
			Закончен
		<? }?>
	</div>
	<div>
		<a href="/hauptmeister/contest/contestedit/<?=$reihe->id ?>">Edit</a> |
		<a href="/hauptmeister/contest/catedit/erstellen/<?=$reihe->id ?>">Новая категория</a> |
		<a href="/hauptmeister/contest/tries/contest/<?=$reihe->id ?>">Попытки</a> |
		<a href="/hauptmeister/contest/posts/contest/<?=$reihe->id ?>">Посты</a> |
		<a href="/hauptmeister/contest/editrating/<?=$reihe->id ?>">Управление рейтингом</a> |
		<?=!$reihe->is_active ? 'Неактивен' : 'Активен' ?>
	</div>
	<? if($reihe->katzen){ ?>
		<? foreach($reihe->katzen as $kater){ ?>
		<div class="h-zpage">
			<h4>Kategorie: <?=$kater->name_cat?></h4>
			<a href="/hauptmeister/contest/catedit/vorzeigen/<?=$kater->id ?>">Edit</a> |
			<a href="?<?=rtl_get_merge_parameters(['showhidden'], ['moveup_cat'=>$kater->id], TRUE)?>">Вверх</a> |
			<a href="?<?=rtl_get_merge_parameters(['showhidden'], ['movedw_cat'=>$kater->id], TRUE)?>">Вниз</a> |
			<a href="/hauptmeister/contest/contesttaskedit/erstellen/<?=$kater->id ?>">Новая таска</a> |
			<a href="/hauptmeister/contest/tries/cat/<?=$kater->id ?>">Попытки</a> |
			<a href="/hauptmeister/contest/posts/cat/<?=$kater->id ?>">Посты</a>
			<? if($kater->tuzik){ ?>
				<table class="h-getable">
					<tr>
						<th>ID Used</th>
						<th>ID Orig</th>
						<th>Title used</th>
						<th>Title original</th>
						<th>Points</th>
						<th>Flag</th>
						<th>Actions</th>
					</tr>
					<? foreach($kater->tuzik as $muzik){ ?>
					<tr class="<?=$muzik->is_removed || $muzik->is_task_removed ? 'tr-wiped' : ''?> <?=!$muzik->is_active || !$muzik->is_task_active ? 'tr-inaktiven' : ''?>">
						<td><?=$muzik->id_tuzik ?></td>
						<td><?=$muzik->id_task ?></td>
						<td><?=$muzik->name_use ?></td>
						<td><?=$muzik->name_task ?></td>
						<td><?=$muzik->points ?></td>
						<td><?=$muzik->tflag ?></td>
						<td>
							<a href="/hauptmeister/contest/contesttaskedit/vorzeigen/<?=$muzik->id_tuzik ?>">Edit usage</a> |
							<a href="/hauptmeister/contest/taskedit/<?=$muzik->id_task ?>">Edit task</a> |
							<a <? if($reihe->is_gehen){?> onclick="return confirm('ВНИМАНИЕ!!! Изменение порятка тасок в категории у идущего контеста может НЕОБРАТИМО нарушить ход соревнования, и доступ юзеров к таскам. Вы точно хотите это сделать?');" <?} ?> href="?<?=rtl_get_merge_parameters(['showhidden'], ['moveup_tuz'=>"$muzik->id_tuzik"], TRUE)?>">Вверх</a> |
							<a <? if($reihe->is_gehen){?> onclick="return confirm('ВНИМАНИЕ!!! Изменение порятка тасок в категории у идущего контеста может НЕОБРАТИМО нарушить ход соревнования, и доступ юзеров к таскам. Вы точно хотите это сделать?');" <?} ?> href="?<?=rtl_get_merge_parameters(['showhidden'], ['movedw_tuz'=>"$muzik->id_tuzik"], TRUE)?>">Вниз</a> |
							<a href="/hauptmeister/contest/tries/task/<?=$muzik->id_tuzik ?>">Попытки</a> |
							<a href="/hauptmeister/contest/posts/task/<?=$muzik->id_tuzik ?>">Посты</a>
						</td>
					</tr>
					<? } ?>
				</table>
			<? }else{?>
				<div>Тасок нету</div>
			<? }?>
		</div>
		<? }?>
	<? }else{ ?>
		<div>Нихрена нет</div>
	<? } ?>
</div>
<? } ?>

<?=tpl_load('print-gemeister.php', array(
	'messages' => $messages
))?>