<h1><?=CLibContest::$current_contest->name_contest?></h1>

<table class="z-table z-rating">
	<tr>
		<th rowspan="2">#</th>
		<th rowspan="2">Команда</th>
		<th rowspan="2">=</th>
		
		<? foreach($das_contest->katzen as $kater){?>
			<th colspan="<?=count($kater->tuzik);?>">
				<?=$kater->name_cat?>
			</th>
		
			<? foreach($kater->tuzik as $tuz){/*?>
			<th>
				<div><?=$tuz->name_use?></div>
				<div><?=$tuz->points?></div>
			</th>
			<? */}?>
		<? }?>
	</tr>
	<tr class="h-spanned">
		<? foreach($das_contest->katzen as $kater){?>

			<? foreach($kater->tuzik as $tuz){?>
			<th class="z-tuzik-bobik">
				<div><?=$tuz->points?></div>
				<div class="z-tuzik" title="<?=$tuz->name_use ? $tuz->name_use : $tuz->name_task?>"><?=$tuz->name_use ? $tuz->name_use : $tuz->name_task?></div>
			</th>
			<? }?>
		<? }?>
	</tr>
	<?
		$pls = 1;
	?>
	<? foreach($das_rating as $reihe){?>
	<tr class="<?=CLibUser::$profile && (int) CLibUser::$profile->id === (int) $reihe->id ? 'z-selber' : ''?>">
		<td class="z-placung">
			<?=$pls++ ?>
		</td>
		<td class="z-mannschaft">
			<? if($reihe->logo_path){?>
			<div><img src="<?=$reihe->logo_path?>" class="z-logo" /></div>
			<? }?>
			<div><?=$reihe->name_team?></div>
		</td>
		<td><b><?=$reihe->n_points?></b></td>
		
		<? foreach($das_contest->katzen as $kater){?>
			<? foreach($kater->tuzik as $tuz){?>
				<? if($zell = $reihe->sorbat[(int) $tuz->id_tuzik]){?>
					<? if($zell->is_success){?>
						<td>
							<div class="z-erfolgreich">+<?=$zell->n_tries > 1 ? $zell->n_tries - 1 : ''?></div>
							<div class="z-tuzik"><?=rtl_utils_date_interval((int) $zell->solve_moment, ':', NULL, O_DALW_HOUR | O_DALW_MINUTE | O_DINT_DAY | O_DINT_HOUR | O_DINT_MINUTE)?></div>
						</td>
					<? }else{?>
						<td>
							<div class="z-fehler">-<?=$zell->n_tries?></div>
						</td>
					<? }?>
				<? }else{?>
				<td>&nbsp;</td>
				<? }?>
			<? }?>
		<? }?>
	</tr>
	<? }?>
</table>