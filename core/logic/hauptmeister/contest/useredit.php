<?

lib_load('meister.php');
lib_load('users.php');
lib_load('contest.php');

$id_team = 0;
$id_user = 0;
$u_action = NULL;

if(count(CKernel::$request) >= 2){

	$u_action = array_shift(CKernel::$request);
	
}else{
	echo "Eure Mutter ist eine Schlampe";
	exit;
}


switch($u_action){
	case 'vorzeigen': {
		$id_user = (int) array_shift(CKernel::$request);
	} break;
		
	case 'erstellen': {
		$id_team = (int) array_shift(CKernel::$request);
	} break;

	default: {
		echo "Eure Mutter ist eine Hure";
		exit;
	} break;	
}

$wata = [
	'is_active' => 1
];


if($_POST['aufstellen_cuser']){
	
	$guten = TRUE;
	
	$die_fielde = [
		'lg'
	];
	
	foreach($die_fielde as $zeile){
		$wata[$zeile] = $_POST[$zeile];
	}
	
	$wata['is_active'] = !!$_POST['is_active'];
	$wata['is_removed'] = !!$_POST['is_removed'];
	
	if(strlen($pushok = $_POST['ph_0']) > 0){
		if($_POST['ph_0'] === $_POST['ph_1']){
			$wata['ph'] = CLibUser::HashPasswort($_POST['ph_0']);
		}else{
			CKernel::KeRegisterMessage('А не совпадают, пароли-то ваши', CKernel::ERROR_LEVEL_WARNING);
			$guten = FALSE;
		}
	}
	
	if($guten){
		if('vorzeigen' === $u_action){
			$jaja = TRUE;

			if($jaja){

				if(rtl_database_update('ctf_cusers', $wata, [
					"id=$id_user"
				])){
					CKernel::KeRegisterMessage('Юзер перепилен', CKernel::ERROR_LEVEL_INFO);
					if($wata['ph']){
						CKernel::KeRegisterMessage('Пароль юзера изменен: <b>' . $wata['lg'] . ':' . $_POST['ph_0'] . '</b>', CKernel::ERROR_LEVEL_INFO);
					}
				}else{
					CKernel::KeRegisterMessage('Лыжи не едут', CKernel::ERROR_LEVEL_WARNING);
				}
			}

		}else if('erstellen' === $u_action){

			$wata['id_team'] = $id_team;

			$jaja = TRUE;
			
			if(!$wata['ph']){
				CKernel::KeRegisterMessage('Ну, новому юзеру типа пароль надо сделать', CKernel::ERROR_LEVEL_WARNING);
				$jaja = FALSE;
			}

			$gaz = rtl_database_select([
				'das' => 'COUNT(*)'
			], 'ctf_cusers', FALSE, [
				"id_team=$wata[id_team]"
			], FALSE, FALSE, 0, 0, $durak = NULL, O_SELECT_ONE_VALUE);

			if($gaz){
				$jaja = FALSE;
				CKernel::KeRegisterMessage('Нельзя создать несколько юзеров для одной команды!', CKernel::ERROR_LEVEL_WARNING);
			}

			if($jaja){
				if($id_user = rtl_database_insert('ctf_cusers', $wata)){
					CKernel::KeRegisterMessage('Юзер запилен: <b>' . $wata['lg'] . ':' . $_POST['ph_0'] . '</b>', CKernel::ERROR_LEVEL_INFO);
					CKernel::KeSaveMessages();
					rtl_local_redirect("/hauptmeister/contest/useredit/vorzeigen/$id_user", 302);
					exit;
				}else{
					CKernel::KeRegisterMessage('Лыжи не едут', CKernel::ERROR_LEVEL_WARNING);
				}
			}
		}else{
			echo "Du gefällst mir nicht";
			exit;
		}
	}
	
}

$der_nutzer = !$_POST['aufstellen_cuser'] && $id_user ? CLibContest::LoadCUsersAllesDamit($id_user, TRUE)[0] : (object) $wata;

CKernel::KeRestoreMessages();

rtl_reply_html(tpl_load('layout/layout_hauptmeister.php', array(
	'title' => 'CTF :: Contest :: ' . ($id_user ? "Редактируем юзера №$id_user" : "Создаём юзера для команды №$id_team"),
	'content' => tpl_load('pages/' . implode('/', CKernel::$reqpath) . '.php', array(
		'der_nutzer' => $der_nutzer,
		'messages' => CKernel::KeGetMessages()
	))
)));