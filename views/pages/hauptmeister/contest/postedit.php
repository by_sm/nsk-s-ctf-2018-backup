<?=tpl_load('contest-menuet.php')?>
<?=tpl_load('print-gemeister.php', array(
	'messages' => $messages
))?>
<form action="" method="post">
	<div>
		<table class="h-forme">
			<tr>
				<td>Название:</td>
				<td><input type="text" name="heading" value="<?=htmlspecialchars(trim($das_post->heading))?>" /></td>
			</tr>
			<tr>
				<td>Текст:</td>
				<td>
					<textarea class="tiny-mce" name="postbody"><?=htmlspecialchars(trim($das_post->postbody))?></textarea>
				</td>
			</tr>
			<tr>
				<td>Дата поста:</td>
				<td><input type="text" name="date_post" value="<?=htmlspecialchars(trim($das_post->date_post))?>" /></td>
			</tr>
			<tr>
				<td>Удален:</td>
				<td><input type="checkbox" name="is_removed" value="eins" <?=$das_post->is_removed ? 'checked="checked"' : ''?> /></td>
			</tr>
			<tr>
				<td><input type="submit" name="aufstellen_post" value="Aufstellen" /></td>
			</tr>
		</table>
	</div>
</form>
<?=tpl_load('print-gemeister.php', array(
	'messages' => $messages
))?>