<?

$das_rating =  CLibContest::LoadRatingTable(CLibContest::$current_contest->id);

rtl_reply_html(tpl_load('layout/layout_contest_freeview.php', array(
	'refresh_time' => (int) $_GET['refresh'],
	'title' => CLibContest::$current_contest->name_contest,
	'content' => tpl_load('pages/' . implode('/', CKernel::$reqpath) . '.php', array(
		'das_rating' => $das_rating,
		'das_contest' => CLibContest::$current_contest,
		'die_postern' => CLibContest::LoadContestPostsVorDer(1, (int) CLibContest::$current_contest->id)
	))
)));