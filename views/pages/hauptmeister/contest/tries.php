<?=tpl_load('contest-menuet.php')?>
<?=tpl_load('print-gemeister.php', array(
	'messages' => $messages
))?>

<div>
<? if($_GET['showhidden']){?>
<a href="?<?=rtl_get_merge_parameters([], ['showhidden'=>0], TRUE)?>">Скрыть удалённые</a>
<? }else{?>
<a href="?<?=rtl_get_merge_parameters([], ['showhidden'=>1], TRUE)?>">Показать удалённые</a>
<?}?>
</div>
<div>Последние &mdash; вверху</div>

<table class="h-getable">
	<tr>
		<th>ID</th>
		<th>Date</th>
		<th>Moment</th>
		<th>Team</th>
		<th>Task</th>
		<th>Posted Flag</th>
		<th>Real Flag</th>
		<th>Points</th>
		<th>Actions</th>
	</tr>
	
	<? foreach($die_reihe as $zeile){ ?>
	<tr class="<?=$zeile->is_removed ? 'tr-wiped' : ''?>">
		<td><?=$zeile->id?></td>
		<td><?=$zeile->date_try?></td>
		
		<td><?=rtl_utils_date_interval((int)$zeile->solve_moment, ':', NULL, O_DALW_HOUR | O_DALW_MINUTE | O_DALW_SECOND | O_DINT_DAY | O_DINT_HOUR | O_DINT_MINUTE | O_DINT_SECOND)?></td>
		
		<td><a href="/hauptmeister/contest/tries/cuser/<?=$zeile->id_cuser?>/<?=$zeile->id_contest?>"><?=$zeile->name_team?></a></td>
		<td><?=$zeile->name_task?> (<?=$zeile->name_use?>)</td>
		<td><?=$zeile->try_flag?></td>
		<td><?=$zeile->tflag?></td>
		<td><?=$zeile->points * $zeile->is_success?></td>
		<td>
			<? if($zeile->is_removed){?>
			<a href="?<?=rtl_get_merge_parameters(['showhidden'], ['activate_try'=>(int) $zeile->id], TRUE)?>">Activate</a>
			<? }else{?>
			<a href="?<?=rtl_get_merge_parameters(['showhidden'], ['deactivate_try'=>(int) $zeile->id], TRUE)?>">Deactivate</a>
			<? }?>
		</td>
	</tr>
	<? }?>
</table>

<?=tpl_load('print-gemeister.php', array(
	'messages' => $messages
))?>