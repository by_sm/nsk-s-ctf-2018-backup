<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link rel="stylesheet" type="text/css" href="/css/style-contest.css" />
	<script type="text/javascript" src="//cdn.jsdelivr.net/jquery/2.1.3/jquery.min.js"></script>
	<script type="text/javascript" src="/js/runtime.js"></script>
	<script type="text/javascript" src="/js/main.js"></script>
	<script type="text/javascript" src="/js/Chart.min.js"></script>	
	<? if($refresh_time){?>
	<meta http-equiv="refresh" content="<?=(int) $refresh_time?>">
	<? }?>
<title><?=$title?></title>
</head>
<body>
	<div class="z-korper" align="left" style="width: auto; padding: 20px">
		<?=$content?>
	</div>
</body>
</html>