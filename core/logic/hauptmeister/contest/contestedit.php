<?

lib_load('meister.php');
lib_load('users.php');
lib_load('contest.php');

$id_contest = 0;

if(count(CKernel::$request) > 0){
	$id_contest = (int) array_shift(CKernel::$request);
}

$wata = [
	'date_start' => date('Y-m-d H:i:s', time() + 86400 * 14)
];

if($_POST['aufstellen_contest']){
	
	$die_fielde = [
		'name_contest',
		'date_start',
		'lange',
	];
	
	foreach($die_fielde as $zeile){
		$wata[$zeile] = $_POST[$zeile];
	}
	
	$wata['is_active'] = !!$_POST['is_active'];
	$wata['is_removed'] = !!$_POST['is_removed'];
	
	if($id_contest){

		if(rtl_database_update('ctf_contest', $wata, [
			"id=$id_contest"
		])){
			CKernel::KeRegisterMessage('Контест обновлен', CKernel::ERROR_LEVEL_INFO);
		}else{
			CKernel::KeRegisterMessage('Что-то не пошло', CKernel::ERROR_LEVEL_WARNING);
		}

	}else{
		
		if($id_contest = rtl_database_insert('ctf_contest', $wata)){
			CKernel::KeRegisterMessage('Контест создан', CKernel::ERROR_LEVEL_INFO);
			CKernel::KeSaveMessages();
			rtl_local_redirect("/hauptmeister/contest/contestedit/$id_contest", 302);
			exit;
		}else{
			CKernel::KeRegisterMessage('Что-то не пошло', CKernel::ERROR_LEVEL_WARNING);
		}
	}
}

$das_kontest = $id_contest ? CLibContest::LoadContestAllein($id_contest) : (object) $wata;

CKernel::KeRestoreMessages();

rtl_reply_html(tpl_load('layout/layout_hauptmeister.php', array(
	'title' => 'CTF :: Contest :: ' . ($id_contest ? "Редактируем контест №$id_contest" : "Создаём контест"),
	'content' => tpl_load('pages/' . implode('/', CKernel::$reqpath) . '.php', array(
		'das_kontest' => $das_kontest,
		'messages' => CKernel::KeGetMessages()
	))
)));